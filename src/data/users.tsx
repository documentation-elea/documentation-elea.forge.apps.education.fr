/**
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

/* eslint-disable global-require */

import {translate} from '@docusaurus/Translate';
import {sortBy} from '@site/src/utils/jsUtils';

/*
 * ADD YOUR SITE TO THE DOCUSAURUS SHOWCASE
 *
 * Please don't submit a PR yourself: use the Github Discussion instead:
 * https://github.com/facebook/docusaurus/discussions/7826
 *
 * Instructions for maintainers:
 * - Add the site in the json array below
 * - `title` is the project's name (no need for the "Docs" suffix)
 * - A short (≤120 characters) description of the project
 * - Use relevant tags to categorize the site (read the tag descriptions on the
 *   https://docusaurus.io/showcase page and some further clarifications below)
 * - Add a local image preview (decent screenshot of the Docusaurus site)
 * - The image MUST be added to the GitHub repository, and use `require("img")`
 * - The image has to have minimum width 640 and an aspect of no wider than 2:1
 * - If a website is open-source, add a source link. The link should open
 *   to a directory containing the `docusaurus.config.js` file
 * - Resize images: node admin/scripts/resizeImage.js
 * - Run optimizt manually (see resize image script comment)
 * - Open a PR and check for reported CI errors
 *
 * Example PR: https://github.com/facebook/docusaurus/pull/7620
 */

// LIST OF AVAILABLE TAGS
// Available tags to assign to a showcase site
// Please choose all tags that you think might apply.
// We'll remove inappropriate tags, but it's less likely that we add tags.
export type TagType =
  | 'énouveautés'
  | 'evaluer'
  | 'entrainer'
  | 'accompagner'
  | 'différencier'
  | 'collaborer'
  | 'produire'
  | 'animer'
  | 'mémoriser'
  | 'exterieur';

// Add sites to this list
// prettier-ignore
const Users: User[] = [
    {
    title: 'Un parcours différencié en SNT',
    description: 'Un parcours qui s\'adapte au niveau de l\'élève en amont d\'une tâche finale',
    preview: require('/docs/Usages/img/differencier_snt_vignette.png'),
    website: '/docs/Usages/differencier-snt',
    tags: ['entrainer', 'différencier'],
  },
    {
    title: 'Favoriser la mémorisation avec un Test',
    description: 'Afin d\'aider les élèves à mémoriser des notions entre deux séances, l\'activité Test d\'Éléa est tout à fait adaptée.',
    preview: require('/docs/Usages/img/Favoriser_la_memorisation_avec_un_Test_vignette.jpg'),
    website: '/docs/Usages/Favoriser_la_memorisation_avec_un_Test',
    tags: ['mémoriser', 'evaluer'],
  },
  {
    title: 'Développer l\'autonomie en CE2',
    description: 'Permettre aux élèves de découvrir de nouvelles notions en autonomie et pour personnaliser les rythmes d\'apprentissage.',
    preview: require('/docs/Usages/img/Developper-autonomie-ce2_vignette.png'),
    website: '/docs/Usages/Developper-autonomie-ce2',
    tags: ['différencier', 'animer'],
  },
  {
    title: 'Évaluer une production orale avec une grille',
    description: 'l\'activité Devoir offre la possibilité de ramasser des productions orales réalisées par les élèves et de les évaluer facilement au moyen d\'une grille d\'évaluation.',
    preview: require('/docs/Usages/img/Evaluer_une_production_orale_avec_une_grille_vignette.jpg'),
    website: '/docs/Usages/Evaluer_une_production_orale_avec_une_grille',
    tags: ['produire','evaluer'],
  },
  /*
  Pro Tip: add your site in alphabetical order.
  Appending your site here (at the end) is more likely to produce Git conflicts.
   */
];

export type User = {
  title: string;
  description: string;
  preview: string | null; // null = use our serverless screenshot service
  website: string;
  tags: TagType[];
};

export type Tag = {
  label: string;
  description: string;
  color: string;
};

export const Tags: {[type in TagType]: Tag} = {
  nouveautés: {
    label: translate({message: 'Nouveautés'}),
    description: translate({
      message:
        'Nouveaux partages d\'usages',
      id: 'showcase.tag.nouveau.description',
    }),
    color: '#e9669e',
  },

  evaluer: {
    label: translate({message: 'Évaluer'}),
    description: translate({
      message: 'Évaluer des travaux d\'élèves dans moodle',
      id: 'showcase.tag.evaluer.description',
    }),
    color: '#39ca30',
  },

  entrainer: {
    label: translate({message: 'Faire s\’entraîner'}),
    description: translate({
      message: 'Faire s’entraîner les élèves',
      id: 'showcase.tag.entrainer.description',
    }),
    color: '#dfd545',
  },

  accompagner: {
    label: translate({message: 'Accompagner les élèves'}),
    description: translate({
      message:
        'Accompagner les élèves “éloignés” de la classe',
      id: 'showcase.tag.accompagner.description',
    }),
    color: '#a44fb7',
  },

  différencier: {
    label: translate({message: 'différencier'}),
    description: translate({
      message:
        'Mettre en œuvre la différenciation pédagogique',
      id: 'showcase.tag.différencier.description',
    }),
    color: '#127f82',
  },

  collaborer: {
    label: translate({message: 'Faire collaborer'}),
    description: translate({
      message:
        'Faire collaborer les élèves',
      id: 'showcase.tag.collaborer.description',
    }),
    color: '#fe6829',
  },

  produire: {
    label: translate({message: 'Faire produire'}),
    description: translate({
      message:
        'Faire créer, produire, et partager les élèves',
      id: 'showcase.tag.produire.description',
    }),
    color: '#8c2f00',
  },

  animer: {
    label: translate({message: 'Animer la classe'}),
    description: translate({
      message: 'Animer la classe',
      id: 'showcase.tag.animer.description',
    }),
    color: '#4267b2',
  },

  mémoriser: {
    label: translate({message: 'Faire mémoriser'}),
    description: translate({
      message:
        'Faire mémoriser',
      id: 'showcase.tag.mémoriser.description',
    }),
    color: '#14cfc3',
  },

  exterieur: {
    label: translate({message: 'hors la classe'}),
    description: translate({
      message:
        'Faire travailler hors la classe',
      id: 'showcase.tag.exterieur.description',
    }),
    color: '#ffcfc3',
  },
};

export const TagList = Object.keys(Tags) as TagType[];
function sortUsers() {
  let result = Users;
  // Sort by site name
  result = sortBy(result, (user) => user.title.toLowerCase());
  // Sort by favorite tag, favorites first
  result = sortBy(result, (user) => !user.tags.includes('favorite'));
  return result;
}

export const sortedUsers = sortUsers();
