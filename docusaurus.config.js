// @ts-check
// `@type` JSDoc annotations allow editor autocompletion and type checking
// (when paired with `@ts-check`).
// There are various equivalent ways to declare your Docusaurus config.
// See: https://docusaurus.io/docs/api/docusaurus-config

import {themes as prismThemes} from 'prism-react-renderer';

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Documentation pour l\'utilisation d\'Éléa',
  tagline: 'Démo',
  favicon: 'img/elea.ico',

  // Set the production url of your site here
  url: 'https://documentation.elea.apps.education.fr/',
  // Set the /<baseUrl>/ pathname under which your site is served
  // For GitHub pages deployment, it is often '/<projectName>/'
  baseUrl: '/',

  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',

  // Even if you don't use internationalization, you can use this field to set
  // useful metadata like html lang. For example, if your site is Chinese, you
  // may want to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'fr',
    locales: ['fr'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: './sidebars.js',
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
         },
        blog: {
          showReadingTime: true,
          blogSidebarTitle:'Derniers articles'
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
        },
        theme: {
          customCss: './src/css/custom.css',
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      // Replace with your project's social card
      image: 'img/elea.png',
      navbar: {
        title: '',
        logo: {
          alt: 'logo Éléa',
          src: 'img/elea.png',
        },
        items: [
          {to: 'Tutoriels', position: 'left', label: 'Tutoriels',},
          {to: 'showcase', label: 'Usages', position: 'left'},
          {
            type: 'doc',
            docId: 'FAQ',
            label: '❓FAQ',
            position: 'left',
          },
          {
            label: 'En régions',
            type: 'dropdown',
            className: 'dyte-dropdown resources-dropdown',
            position: 'left',
            items: [
              {type : 'doc', docId : 'EnRegions/AuRA/index', label: 'Aura'},
              {type : 'doc', docId : 'EnRegions/Bretagne/index', label: 'Bretagne'},
              {type : 'doc', docId : 'EnRegions/IDF/index', label: 'IDF'},
            ],
          },
          {to: 'blog', label: 'Nouveautés', position: 'left'},
        ],

      },
      algolia: {
        // L'ID de l'application fourni par Algolia
        appId: 'D8X21ZDUOW',
  
        // Clé d'API publique : il est possible de la committer en toute sécurité
        apiKey: 'bcddf88fcc7d6c7fb89990dc4d1e3fd2',
  
        indexName: 'dne-elearning-gitlab',
  
        // Facultatif : voir la section doc ci-dessous
        contextualSearch: true,

        // Facultatif : paramètres de recherche de Algolia
        searchParameters: {},
  
        // Facultatif : chemin pour la page de recherche qui est activée par défaut (`false` pour le désactiver)
        searchPagePath: 'search',
  
        //... autres paramètres de Algolia
      },
      footer: {
        style: 'dark',
        links: [{
           title: 'Articles',
           items: [
            {
              label: 'Présentation de la plateforme Éléa (Versailles)',
              href: 'https://ressources.dane.ac-versailles.fr/ressource/elea',
            },
           ],
          },
           {
            title: 'Communautés nationales',
            items: [
              {
                label: 'Inter-académique Moodle',
                href: 'https://iam.unistra.fr',
              },
              
            ],
          },
          {
            title: 'Communautés locales',
            items: [
              {
                label: 'Échanger entre pairs (Versailles)',
                href: 'https://magistere.education.fr/ac-versailles/course/view.php?id=16570',
              },
              {
                label: 'Échanger entre pairs (Bretagne)',
                href: 'https://magistere.education.fr/ac-rennes/course/view.php?id=8377',
              },
              {
                label: 'Échanger entre pairs (AuRA)',
                href: 'https://magistere.education.fr/ac-clermont/course/view.php?id=4607',
              },
            ],
          },
        ],
        copyright: `Équipe Éléa nationale, Ministère de l'Éducation nationale et de la Jeunesse `,
      },
      prism: {
        theme: prismThemes.github,
        darkTheme: prismThemes.dracula,
      },
    }),

  plugins: [
    [
      '@docusaurus/plugin-ideal-image',
      {
        quality: 70,
        max: 1030, // max resized image's size.
        min: 640, // min resized image's size. if original is lower, use that size.
        steps: 2, // the max number of images generated between min and max (inclusive)
        disableInDev: false,
      },
    ],
  ],
};

export default config;
