# Découvrir et récupérer des parcours de la Éléathèque

## Étape 1 : Accéder à la Éléathèque

Pour accéder à la Éléathèque, il est nécessaire de se connecter à la plateforme et de s'identifier.

![page d'accueil élea](../img/eleatheque/page_accueil.png#printscreen#centrer)    ![page d'accueil élea](../img/eleatheque/connexion.png#printscreen#centrer)

Sur votre tableau de bord dans la barre du haut, cliquez sur "Éléathèque".

![tableau de bord](../img/eleatheque/acceder_eleatheque.png#printscreen#centrer)


## Étape 2 : Affiner sa recherche par mots-clés

Pour affiner votre recherche par niveau, discipline, approche pédagogique ou thématique.
Vous pouvez :
* sélectionner un des mots-clés les plus fréquemment utilisés dans la liste à droite de l'écran :
![mots-clés](../img/eleatheque/parcourir_eleatheque.png#printscreen#centrer)
* taper votre recherche dans le champ texte (des propositions s'affichent en-dessous selon les caractères entrés) :
![recherche libre](../img/eleatheque/parcourir_eleatheque2.png#printscreen#centrer)

Puis cliquez sur "Valider"

Une sélection de parcours correspond à votre recherche s'affiche comprenant le titre du parcours et l'ensemble des mots-clés associés.

![sélection de parcours](../img/eleatheque/selection_parcours.png#printscreen#centrer)

## Étape 3 : Découvrir le contenu d'un parcours

Cliquez sur le titre du parcours pour obtenir des informations complémentaires :
* sur la partie gauche s'affiche le scénario du parcours avec l'ensemble des activités.
* sur la partie droite s'affiche une description du parcours et le nom des auteurs.

![exemple de présentation de parcours](../img/eleatheque/exemple_parcours.png#printscreen#centrer)

### Si vous possédez un compte sur la plateforme Communauté de Versailles :

Vous pouvez tester le parcours sur le bouton "tester le parcours" ou en cliquant sur la première activité du parcours, vous accédez à la plateforme https://communaute.elea.ac-versailles.fr/ où sont stockés les parcours exemples académiques.
Sur cette plateforme, vous devez vous identifier avec votre compte académique ou avec un compte local si vous en possédez un.
Puis, vous pouvez consulter directement le parcours que vous avez choisi avec un statut d'enseignant non-éditeur.
Vous avez accès à toutes les parties y compris celles cachées aux élèves et vous n'êtes pas bloqué dans votre progression. Par contre, vous ne pouvez pas modifier le parcours.

![tester le parcours](../img/eleatheque/tester_parcours.png#printscreen#centrer)

Pour tester le parcours comme les élèves, cliquez sur votre menu d'utilisateur puis sur "Prendre le rôle de..." et enfin choisissez "ÉTUDIANT".

![arole](../img/eleatheque/role.png#printscreen#centrer)

En cliquant sur une activité, vous accédez à la page correspondante sur le parcours sans avoir à réaliser la progression demandée aux élèves.


## Étape 4 : Récupérer un parcours

Si vous souhaitez récupérer un parcours pour le proposer à vos élèves tel quel ou le personnaliser, vous pourrez le télécharger depuis la Éléathèque.

Pour récupérer le parcours actualisé, veuillez suivre la procédure suivante :

Cliquez sur « Télécharger le parcours ».

![télécharger un parcours](../img/eleatheque/accueil.png#printscreen#centrer)

Vous récupérez un fichier "Course.mbz" que vous pouvez enregistrer sur votre ordinateur.
Vous pouvez maintenant restaurer le parcours sur votre plateforme de bassin.

Créez un nouveau parcours vide et donnez lui un nom.

![parcours-vide](../img/eleatheque/parcours_vide.png#printscreen#centrer)

Puis dans le menu "Plus" du menu horizontal, "réutilisation de parcours", cliquez sur « restauration ».

![restauration](../img/eleatheque/restauration.png#printscreen#centrer)

Vous accédez au gestionnaire des sauvegardes dans lequel vous allez pouvoir envoyer votre fichier récupéré. 

![restauration2](../img/eleatheque/restauration2.png#printscreen#centrer)

Vous pouvez vous référer à la procédure de [migration d'un parcours](https://dne-elearning.gitlab.io/moodle-elea/documentation/docs/Professeurs/Acceder%20aux%20plateformes%20et%20aux%20parcours/Faire_migrer_un_parcours/ "tutoriel pour migrer un parcours") à partir de l'étape de 2 en cas de difficulté.

Sur ce nouveau parcours restauré, identique au parcours exemple académique, vous avez des droits d'enseignant éditeur et pouvez le partager à vos classes et le modifier. Vous pouvez aussi ne restaurer qu'une partie du parcours si vous le désirez.

## Étape 5 : Récréer les badges

Si le parcours contient des badges, ils ne font pas partie de la sauvegarde et ils doivent être ajoutés manuellement.

Pour cela, rendez-vous sur votre tableau de bord, et sur la ligne correspondante au parcours, cliquer sur l'icône "Ajouter des badges".

![badge1](../img/eleatheque/badge1.png#printscreen#centrer)

Pour savoir quel badge mettre, quel titre lui donner et à quelle activité l'attribuer, retourner sur la éléathèque. Dans la partie basse de la description du parcours, vous allez retrouver l'image à récupérer et le chiffre en dessous va indiquer l'activité qu'il faut réussir pour l'obtenir.

![badge2](../img/eleatheque/badge2.png#printscreen#centrer)

Il ne vous reste plus qu'à saisir ces différentes informations pour que les élèves se voient attribuer un badge lorsqu'ils réussiront l'activité.
