# Dupliquer un parcours sur la même plateforme

## Etape 1 : Sauvegarder le parcours à dupliquer

Informations à recueillir avant de commencer :
- Connaître le nombre de sections du parcours
- Savoir si des badges ont été insérés dans le parcours car la procédure de duplication ne permet pas de les remettre automatiquement. Il conviendra de les rajouter manuellement.

Sélectionnez le parcours à dupliquer et ouvrez-le. 
Cliquez sur le bouton bleu en haut à droite pour ouvrir le menu 
![admin-icon](../img/migrer_un_parcours/capture0.png#printscreen#centrer)

Dans le menu, sélectionnez "Administration du cours" 
![admin-icon](../img/migrer_un_parcours/capture1.png#printscreen#centrer)

Pour finir, sélectionnez "sauvegarde" 
![admin-icon](../img/migrer_un_parcours/capture2.png#printscreen#centrer)

Descendez en bas de la page et cliquez sur "Sauter à la dernière étape".
Attendez quelques secondes pendant la sauvegarde.
Le message en rouge n'est pas bloquant.

Cliquez sur "Continuer" pour accéder à la zone de sauvegarde privée où vous verrez le fichier sauvegardé de votre parcours.

## Etape 2 : Restaurer le parcours
Sur le tableau de bord de la plateforme, cliquez sur "Ajouter un nouveau parcours".

![admin-icon](../img/migrer_un_parcours/parcours_vide.png#printscreen#centrer)

Sélectionnez "Parcours vide".

![admin-icon](../img/migrer_un_parcours/parcours_vide2.png#printscreen#centrer)

Donnez-lui un nom différent du parcours modèle, choisissez un dossier de destination, puis cliquez sur "Créer le parcours".

Rendez-vous dans ce nouveau parcours

![admin-icon](../img/migrer_un_parcours/capture0.png#printscreen#centrer)

Sélectionnez dans le menu "Plus", le menu "Réutilisation de cours"

![admin-icon](../img/migrer_un_parcours/capture1.png#printscreen#centrer)

puis sélectionner le menu "Restauration"

![admin-icon](../img/migrer_un_parcours/capture3.png#printscreen#centrer)

Dans la zone de sauvegarde privée, sélectionnez le fichier sauvegardé et cliquez sur "Restauration"
![admin-icon](../img/migrer_un_parcours/capture2aa.png#printscreen#centrer)

Cliquez en bas à droite sur "Continuer".

Cochez **impérativement** l'option "Fusionner le cours sauvegardé avec ce cours". Cliquez sur "Continuer".

![admin-icon](../img/migrer_un_parcours/capture7.png#printscreen#centrer)

Validez les pages suivantes en cliquant sur "Suivant" puis sur la dernière page cliquez sur "Effectuer la restauration".
Le temps nécessaire à la restauration est variable en fonction du poids du parcours. 
Un dernier message s'affiche indiquant que le parcours a bien été restauré.

Cliquez sur "Continuer" pour visualiser le nouveau parcours exactement identique au parcours modèle. 

Attention : si le parcours modèle comprend des badges, il convient de les [ajouter](https://dne-elearning.gitlab.io/moodle-elea/documentation/docs/Professeurs/Concevoir%20des%20parcours/Ajouter_un_badge/ "tutoriel pour ajouter un badge") manuellement.


