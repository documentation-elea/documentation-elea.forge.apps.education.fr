---
tags:
  - activité
  - Test
  - quiz
  - évaluer
---
import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';


# Intégrer une activité Test

**L'activité Test permet de concevoir des tests auto-corrigés**, composés de questions variées (choix
unique ou choix multiples, appariement, vrai-faux, réponse courte, réponse numérique, glisser-déposer...)

:::tip Usages pédagogiques possibles  
###### selon la méthode [ABC Learning Design](https://abc-ld.org/tool-wheel/)

![ABC](../img/ABC/vignette_ABC_entrainement.png#abc) : Éléa peut servir à remplacer un QCM sur papier. L'enseignant peut autoriser plusieurs tentatives pour effectuer le test. Il peut aussi choisir de donner un feedback et/ou d'afficher les réponses correctes.	

![ABC](../img/ABC/vignette_ABC_collaboration.png#abc) : Les élèves peuvent concevoir mutuellement des questions. 

:::

## Étape 1 - Ajouter une activité test

<details>
<summary> Tutoriel vidéo (ancienne version d'Éléa)</summary>
<iframe title="e-éducation &amp; Éléa - Intégrer une activité Test dans un parcours (1/3)" width="560" height="315" src="https://tube-numerique-educatif.apps.education.fr/videos/embed/f05bf2c1-5da5-43f4-bbb3-a75b8be3c289" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>
</details>

Pour créer une activité **Test**, ouvrez le parcours sur lequel vous souhaitez ajouter cette activité en mode édition. 

<Tabs queryString="action">
<TabItem value="Depuis la barre d'édition" label="Depuis la barre d'édition">

Dans la barre d'édition du tiroir de droite, cliquez sur le menu "Activités", puis sur l'icône correspondante à l'activité Test.

![Icône test](../img/activite_test/test0.png)

</TabItem>
<TabItem value="Depuis le sélecteur d'activités" label="Depuis le sélecteur d'activités">

- Cliquez sur **"Ajouter une activité ou une ressource"**;

- Sélectionnez l'activité **"test "** dans la liste ;  
![Icône test](../img/activite_test/test0.png)

- Et enfin cliquez sur **"Ajouter"**.  

</TabItem>
</Tabs> 

Une nouvelle page s'ouvre pour définir les paramètres de l'activité.

## Étape 2 - Paramétrez votre activité test

1. Commencez par attribuer un nom à votre activité et si vous le souhaitez une petite description.

![généraux](../img/activite_test/test1.png#printscreen#centrer)

### Autres paramètres importants

2. **Temps** :
    - **Date** : Cochez la case "**Activer**" pour paramétrer les champs Ouvrir ce test et Fermer le test si vous désirez délimiter quand le test sera disponible.
    - **Durée du test** : Cochez la case "**Activer**" à Temps disponible si vous souhaitez limiter le temps alloué pour répondre aux questions du test. Un chronomètre s'affichera lors de la passation du test avec un compte à rebours.

![temps](../img/activite_test/test2.png#printscreen#centrer)

3. **Note** :
    - Sélectionnez le Nombre de tentatives autorisées
    - Sélectionnez à Méthode d'évaluation pour choisir quelle note comptera (par défaut, c'est la note la plus haute).

![temps](../img/activite_test/test3.png#printscreen#centrer)

4. **Comportement de la question** :  
    Définissez comment se comportent les questions quand l'étudiant aura accès au feedback :  
    - Feedback immédiat = après chaque question
    - Feedback a posteriori = le corrigé sera affiché une fois le test envoyé  (c'est l'option par défaut)

![temps](../img/activite_test/test4.png#printscreen#centrer)

5. Cliquez sur le bouton "**Enregistrer** et afficher" pour créer des questions au test (ou en rajouter)

## Étape 3 : Création des questions

1. A la création du test, un avertissement vous informe qu'il n'existe aucune question. Cliquez sur "**Questions**"

![questions](../img/activite_test/test5.png#printscreen#centrer)

### Création de nouvelles questions :

![temps](../img/activite_test/test6.png#printscreen#centrer)

2. Cliquez sur le bouton "**Ajouter**" puis sur "**+ une question**"

:::tip Bon à savoir 
Vos questions sont stockées dans une "**Banque de questions**".  
Elles pourront être réutilisées dans d'autres Tests
:::

Le sélecteur "**Choisir un type de question à ajouter**" s'affiche.

![temps](../img/activite_test/test7.png#printscreen#centrer)

3. Cliquez sur le **type de question à ajouter**, puis sur le bouton "**Suivant**".

:::tip Bon à savoir 
Les principaux types de questions sont décrites dans la page [L'activité Test : types de questions](activite_test_type_questions)
:::


4. Après la création d'une question, elle apparaît dans la page de modification du test ; cliquez sur "**Ajouter**" puis sur "**+ une question**" pour continuer à créer des questions.

### Mise en forme du test :
![temps](../img/activite_test/test8.png#printscreen#centrer)


**icônes:**
- ![icone](../img/deplacer.png) Réorganiser l'ordre des questions
- ![icone](../img/SautPage.png) Ajouter ou suprimer un saut de page
- ![icone](../img/engrenage.png) Modifier la question
- ![icone](../img/loupe.png) Prévisualiser la question
- ![icone](../img/corbeille.png) Supprimer une question
- ![icone](../img/stylo.png) Modifier le coefficient d'une question

:::danger Tentatives
Pour modifier un test dans lequel des élèves ont déjà répondu, vous devez supprimer leurs tentatives.
:::

### Paramétrer des feedbacks (en fonction des réponses)

<details>
<summary> Tutoriel vidéo (ancienne version d'Éléa)</summary>
<iframe title="e-éducation &amp; Éléa - Intégrer une activité Test dans un parcours (2/3)" width="560" height="315" src="https://tube-numerique-educatif.apps.education.fr/videos/embed/3f25128f-4d85-4ae6-a1a5-137121678f2a" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>
</details>

### Prévisualisation du test

1. Revenez dans le cours et **cliquez sur votre activité**.

![previsualiser](../img/activite_test/test9.png#printscreen#centrer)

2. Cliquez sur le bouton "**Prévisualiser le test maintenant**" ou sur l'Administration du test (engrenage) et le menu "**Prévisualisation**".

![previsualiser](../img/activite_test/test10.png#printscreen#centrer)

## Récupérer les résultats des élèves et corriger leurs réponses

<details>
<summary> Tutoriel vidéo (ancienne version d'Éléa)</summary>
<iframe title="e-éducation &amp; Éléa - Intégrer une activité Test dans un parcours (3/3)" width="560" height="315" src="https://tube-numerique-educatif.apps.education.fr/videos/embed/e5b8a6ea-93b6-461a-a40e-222c40e36af2" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>
</details>