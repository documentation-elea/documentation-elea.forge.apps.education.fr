# Paramétrer des activités : suivi d'achèvement et restrictions d'accès

<iframe title="e-éducation &amp; Éléa - Paramétrer les restrictions d'accès dans un parcours Éléa" width="560" height="315" src="https://tube-numerique-educatif.apps.education.fr/videos/embed/fd2a531b-eb80-472f-b208-f5293234f56b" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

<iframe title="e-éducation &amp; Éléa - Paramétrer des restrictions d'accès imbriquées" width="560" height="315" src="https://tube-numerique-educatif.apps.education.fr/videos/embed/a9e03285-980d-48b2-9dc6-362b4754fed3" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>
