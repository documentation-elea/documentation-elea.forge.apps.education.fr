# Intégrer une activité Sondage

<iframe title="e-éducation &amp; Éléa - Intégrer une activité Sondage dans un parcours" width="560" height="315" src="https://tube-numerique-educatif.apps.education.fr/videos/embed/adee409a-93d6-471e-a243-d892973a52ec" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

## Étape 1 - Ajouter une activité Sondage

Pour créer une activité **Sondage**, ouvrez le parcours sur lequel vous souhaitez ajouter cette activité. 

Cliquez en haut à droite sur le bouton "**Activer le mode édition**".

![Sondage-Choix de l'activité](../img/activitesondage/activitesondage01activerlemodeedition.png#printscreen#centrer)

Sélectionnez ensuite la section concernée et :

- Cliquez sur **"Ajouter une activité ou une ressource" **;

![Sondage-Choix de l'activité](../img/activitesondage/activitesondage02ajouteruneactiviteouressource.png#printscreen#centrer)

- Passez en **"Mode Avancé" **;

![Sondage-Choix de l'activité](../img/activitesondage/activitesondage03modeavance.png#printscreen#centrer)

- Sélectionnez l'activité **Sondage **dans la liste ;

![Sondage-Choix de l'activité](../img/activitesondage/activitesondage04choisiractivitesondage.png#printscreen#centrer)

- Et enfin cliquez sur **"Ajouter"**.

Une nouvelle page s'ouvre pour définir les paramètres de l'activité. 

## Étape 2 - Ajouter du contenu dans votre activité Sondage

Commencez par attribuer un nom à votre activité et si vous le souhaitez une petite description.

![Sondage-Titre](../img/activitesondage/activitesondage05ajoutsondageparametres.png#printscreen#centrer)

**Paramétrez** les **options** de votre sondage. Vous avez la possibilité de :
- permettre ou non, la modification du choix ;
- permettre ou non, le choix de plusieurs réponses ;
- limiter ou non, le nombre de réponses permises (lorsque le sondage est utilisé pour permettre aux élèves de se répartir en groupes par exemple).

![Sondage- Options](../img/activitesondage/activitesondage06optionssondage.png#printscreen#centrer)

Vous pouvez ensuite remplir les options du sondage (en indiquant si nécessaire la limite de réponses permises par option).

![Sondage- Options](../img/activitesondage/activitesondage07optionssondagechoix.png#printscreen#centrer)

## Étape 3 - Paramétrer les résultats de votre activité Sondage

- **Paramétrez** les **résultats** de votre sondage. 
  Faites votre choix concernant : 

  - la publication des résultats ;
  ![Sondage- Options](../img/activitesondage/activitesondage08resultatssondage.png#printscreen#centrer)

  - la confidentialité des résultats.
  ![Sondage- Options](../img/activitesondage/activitesondage09resultatssondageconfidentialitee.png#printscreen#centrer)


- **Enregistrez** votre sondage. Il est alors prêt à être utilisé.

- Il est enfin possible de **paramétrer** votre jeu d'appariement (restrictions d'accès, achèvement d'activités...) : [Paramétrer des activités : suivi d'achèvement et restrictions d'accès ](?role=prof&element=integrer-des-activites&item=parametrer-des-activites-suivi-d-achevement-et-restrictions-d-acces).
