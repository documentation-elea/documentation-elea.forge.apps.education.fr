# Créer les comptes utilisateurs avec SIECLE 2nd degré

Cette procédure est réservée aux établissements du second degré ne disposant pas d'ENT ou de connecteur dans l'ENT (établissements privés, ...)

## Étape 1 – Récupérer le fichier généré à partir de SIECLE

Demandez à votre direction de vous fournir le fichier **« élèves sans adresse »** exporté depuis **l’application SIECLE**. Il vous permettra de procéder à la création des comptes utilisateurs des élèves sur la plateforme Éléa de votre établissement.

À partir du portail Arena, choisissez **« Scolarité du 2nd degré »** dans le menu puis **« Consultation et export »**.
Vérifiez l’année sélectionnée puis cliquez sur **« Entrer »**.

![portail_ARENA](img/importsiecle/capture1.png#printscreen#centrer)

Dans le **menu « Exportations »**, choisissez **« En XML »** puis **« Elèves sans adresse »**.

![exportations_xml](img/importsiecle/capture2.png#printscreen#centrer)

Vous obtenez un **fichier compressé** qui peut-être utiliser tel quel sur la plateforme Éléa.

![enregistrer_dossierzip](img/importsiecle/capture3.png#printscreen#centrer)

## Étape 2 – Se connecter à la plateforme Éléa de l'établissement

Entrez l'url correspondant à votre plateforme de bassin pour l'académie de Versailles ou la plateforme de votre zone géographique pour les autres académies.

Cliquez sur « **Choisissez votre compte** » puis sur « **Utiliser mon compte local** ».

![accueil_elea](img/capture2.png#printscreen#centrer)

Puis entrez **l'UAI (RNE) et le mot de passe.**

![accueil_elea](img/capture3.png#printscreen#centrer)

## Étape 3 – Importer vos comptes utilisateurs dans Éléa

Une fois connecté, cliquez sur **Importer des utilisateurs**

Déposer le fichier en cliquant sur **« Choisir un fichier »** ou **glissez/déposez** celui-ci.  

![import](img/importsiecle/capture6.png#printscreen#centrer)
Choisissez entre les deux options proposées en fonction de votre situation.

:::warning **Attention!**

 Pour les établissements ayant déjà accès à la plateforme Éléa, les deux options qui vous sont proposées sont importantes. Vous pouvez :

![import](img/importsiecle/capture6bis.png#printscreen#centrer)

- conserver les mots de passe créés précédemment en cliquant sur le bouton de droite ;
- générer de nouveaux mots de passe pour tous les utilisateurs en cliquant sur le bouton de gauche (option obligatoire si vous n’avez pas conservé les identifiants de l’année précédente ou si vous avez interrompu par mégarde l’importation).
:::

Patientez pendant la création des cohortes et des utilisateurs. La procédure est automatique et ne demande aucune action de votre part.

![creation_cohortes](img/importsiecle/capture7.png#printscreen#centrer)

![creation_cohortes](img/importsiecle/capture7bis.png#printscreen#centrer)

Une fois l'importation terminée, vous pouvez cliquer sur les étapes pour vérifier ce qui a été effectué.  
**vous devez impérativement récupérer les identifiants des utilisateurs au format PDF**.

![enregistrer_pdf](img/importsiecle/capture8.png#printscreen#centrer)
