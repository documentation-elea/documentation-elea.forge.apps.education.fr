# Créer les comptes utilisateurs avec ONDE 1er degré

Vous allez créer les comptes pour tous les élèves de votre école.

La procédure se déroule en deux temps :

1. Récupérer la liste à jour des élèves de votre école sur le portail ONDE (phase d'**exportation**) ;
2. Téléverser cette même liste sur la plateforme Éléa (phase d'**importation**).


## Étape 1 – Récupérer la liste des élèves générée à partir de ONDE

1. Connectez-vous au portail [Arena](https://id.ac-versailles.fr/login/ct_logon_mixte.jsp) avec vos identifiants académiques.

2. Sur "**Mon tableau de Bord**" de votre école, dans le menu déroulant "**Liste & Documents**", choisissez "**Extractions**".

![Tableau de bord ONDE](img/importonde/importonde01montableaudebord.png)

3. Sur l'écran suivant, sélectionnez "**Élèves de l'école ou leurs responsables**".

![Menu Élèves et responsables](img/importonde/importonde02listeeleves.png)

4. Vérifiez que l'option "**Élèves**" est bien cochée par défaut, puis cliquez sur icône **loupe**.

![Option de l'export ONDE : Élèves](img/importonde/importonde03optioneleves.png)

La liste des élèves est alors générée.

![Génération de la liste des élèves](img/importonde/importonde04extraction.png)

Cliquez sur "**Extraction des élèves - format CSV**".

5. La fenêtre surgissante qui apparaît vous invite alors à enregistrer sur votre ordinateur un fichier nommé "**CSVExtraction.csv**". Ce fichier contient la liste complète des élèves de l'école. Cette liste est formatée de telle sorte que la plateforme Éléa pourra ultérieurement créer automatiquement à partir de cette liste un compte individuel pour chaque élève (Étape 3 - voir ci-dessous).

![Enregistrer le fichier CSV](img/importonde/importonde05enregistrementcsv.png)

> **Attention** :
> 
> Notez bien l'emplacement où vous enregistrez sur votre ordinateur ce fichier "**CSVExtraction.csv**", de façon à pouvoir le récupérer pour l'Étape 3. 
> 
> Le formatage de cette liste d'élèves extraite depuis le portail ONDE est spécifiquement adapté pour qu'elle puisse être ensuite réemployée au sein de la plateforme Éléa : aussi **n'ouvrez pas** ce fichier "**CSVExtraction.csv**" téléchargé, et **ne modifiez en aucun cas, ni le contenu de ce fichier, ni même sa dénomination**  (par défaut donc "**CSVExtraction.csv**").
> 
> De plus, ce fichier, qui doit donc impérativement se nommer "**CSVExtraction.csv**" et qui contient des données sur les élèves, devra, pour des questions juridiques, être détruit dès la fin de la procédure.
> 
> **Ne conservez jamais un tel fichier** !

![Enregistrer le fichier CSV](img/importonde/importonde06fichiercsv.png)

## Étape 2 – Se connecter à la plateforme Éléa de l'établissement

1. Dans la barre d'adresse de votre navigateur internet entrez l'url correspondant à votre plateforme de bassin, sur le modèle **https://*BASSIN*.elea.ac-versailles.fr**.

**Exemple** : pour une école située dans le bassin de la ville de Boulogne on saisira donc l'adresse,

https://boulogne.elea.ac-versailles.fr

![Compte Local](img/capture2.png)

**Remarque** : si vous ne connaissez pas le bassin auquel est rattachée votre école, rapprochez-vous de votre référent Éléa (eRUN).

2. Sur l'écran, cliquez sur **« Utiliser mon compte local »**.

3. Votre identifiant est **l'UAI (ancien RNE)** de votre école.

![Connexion](img/capture3.png)

4. **Le mot de passe** : 

Lors de votre toute **première connexion** à votre plateforme Éléa de bassin, cliquez sur le lien qui est inclus dans le mail d'information qui a été envoyé sur l'adresse mail de direction de votre école - adresse du type **ce.*UAI*.@ac-versailles.fr**, où ***UAI*** correspond à l'**UAI** (ancien **RNE**) de votre école. Vous pourrez ainsi générer votre mot de passe.

Vous pouvez dès lors vous connecter à votre plateforme Éléa de bassin en utilisant donc, pour identifiant **l'UAI (ancien RNE)** de votre école, et, comme mot de passe, celui que vous venez de créer (réemployez ces deux éléments de connexion pour toutes vos connexions ultérieures à cette plateforme Éléa).

**Remarque** : le lien envoyé par mail pour la création du mot de passe est valable **30 jours**. Si ce délai est échu ou si vous égarez ce mot de passe, cliquez sur la mention "**Vous avez oublié votre nom utilisateur et/ou votre mot de passe**".

Vous recevrez alors au bout de quelques minutes un nouveau lien à l'adresse mail de direction de votre école pour regénérer votre mot de passe.

## Étape 3 – Créer les comptes des élèves dans Éléa

1. Sur l'écran suivant cliquez sur "**Nouvelle importation**".

![Nouvelle Importation](img/importonde/importonde10importationelea.png)

2. Vous allez à présent téléverser la liste des élèves précédemment exportée depuis le portail ONDE et enregistrée sur votre ordinateur. Pour cela :
![Importation du fichier csv](img/importonde/importonde11importationlisteelea.png)
   - **A** : Soit cliquez sur "**Choisir un fichier**" ; de là parcourez les dossiers de votre ordinateur pour y sélectionner le fichier "**CSVExtraction.csv**" et cliquez sur "**Ouvrir**" dans votre explorateur de fichiers ;
   - **B** : Ou bien, si votre explorateur de fichiers est déjà ouvert et affiche le dossier où a été enregistré le fichier  "**CSVExtraction.csv**" : cliquez sur ce fichier et glissez-déposez-le dans la zone bordée de pointillés prévue à cet effet.

**Attention** : si vous avez déjà effectué précédemment des importations sur la plateforme Éléa de votre école, soyez **très vigilant** aux deux options (verte et orange) qui s'afficheront à ce stade.

![Options des mots de passe](img/importonde/importonde12optionsmotsdepasse.png)

Vous pouvez :

- Soit cliquer sur **le bouton vert** pour conserver les mots de passe déjà créés lors d'une importation précédente ; cette option est à privilégier si, par exemple, des élèves ont rejoint en cours d'année l'école : l'importation en cours va créer de nouveaux comptes pour ces seuls nouveaux élèves, sans pour autant modifier ceux des autres élèves (et donc, avec cette option, sans modifier leurs mots de passe existants) ;

- Ou bien, cliquer sur **le bouton orange** pour générer de nouveaux mots de passe pour **tous** les élèves, de l'école ;  cette option s'imposera si par exemple vous n’avez pas conservé les identifiants de l’année précédente, et donc si vous avez la nécessité de les recréer pour **tous** les élèves - y compris pour ceux qui disposaient déjà d'un accès. Vous cliquerez également sur cette option si vous avez interrompu par erreur une importation.

**Remarque** : toute nouvelle importation vient **compléter** et réactualiser les importations précédentes : vous pouvez donc mettre à jour facilement les comptes de vos élèves sur Éléa en cours d'année (en cas de changement de classe, d'arrivée ou de départ d'un élève). Le fichier "**CSVExtraction.csv**" actualisé que vous récupèrerez sur ONDE sera à chaque fois comparé avec les comptes existants, et, de là, seules les modifications nécessaires seront apportées aux comptes Éléa des élèves concernés.

3. Patientez pendant la création des classes et des comptes des élèves (étape 2 et 3 à l'écran). Cette procédure est automatique et ne demande aucune action particulière de votre part. 

**Remarque** : il arrive que cette procédure d'importation se "bloque" d'elle-même. En ce cas, cliquez simplement sur la flèche de retour en arrière de votre navigateur internet.

![Retour arrière](img/importonde/importonde14retourarrière.png)

Puis, sur l'écran précédent, redéposez comme avant le même fichier "**CSVExtraction.csv**" : la procédure d'importation redémarrera très exactement là où elle s'était bloquée, et elle sera complétée automatiquement avec les seuls comptes élèves et classes restants à créer.

## Étape 4 - Imprimer les mots de passe des élèves

![Impression des mots de passe](img/importonde/importonde15pdfmotsdepasse.png)

Une fois l'importation terminée, **vous devez impérativement récupérer les identifiants des élèves au format PDF**. 

Cliquez sur "**Enregistrer**" ou "**Imprimer**" : vous n'aurez plus qu'à imprimer le fichier .pdf ainsi généré et à découper les coupons prêts à être distribués aux élèves, pour fournir à chacun, son identifiant et son mot de passe personnels de connexion à la plateforme Éléa de votre école.
