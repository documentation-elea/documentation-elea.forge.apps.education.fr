# Procédure de réinitialisation du mot de passe

Si vous avez oublié votre mot de passe, une procédure de réinitialisation est prévue lors de la phase de connexion à éléa :

## Étape 1
1. Rendez-vous sur la page d'accueil de votre plateforme et cliquez sur **"Choisissez votre compte"**

2. Cliquez sur **"Utiliser mon compte local"**

Si vous n'utilisez pas de compte local, cette procédure ne vous concerne pas.

![page_accueil_elea](img/capture2.png#printscreen#centrer)

3. Cliquez sur **"Nom d'utilisateur ou mot de passe oublié ?"**

![page_accueil_elea](img/capture5.png#printscreen#centrer)



## Étape 2

Renseignez votre nom d'utilisateur. Pour un compte établissement, le nom d'utilisateur à renseigner correspond à l'**UAI** de ce dernier. 

:::danger Attention, 

- écrivez la dernière lettre en minuscule

- ne renseignez pas l'adresse mail complète de l'établissement, juste l'UAI.

:::

![recuperation](img/mdp_oubli.png#printscreen#centrer)

Puis, cliquez sur **"RECHERCHER"**


Un mail contenant un lien permettant la réinitialisation du mot de passe sera envoyé sur l'adresse mail de l'établissement.

:::danger **Attention** : Ce lien n'est valable que pendant **30 minutes**.

:::

![mail_lien_réinitialisation](img/mail_lien_reinitialisation.png#printscreen#centrer)

1. Cliquez sur le lien. 

2. Une nouvelle page s'ouvre, cliquez sur **"CONTINUER"** 

![continuer](img/mdp_oubli2.png#printscreen#centrer)

3. Définissez votre nouveau mot de passe puis cliquez sur **"ENREGISTRER"**

![réinitialisation du mot de passse](img/mdp_oubli3.png#printscreen#centrer)

Vous pouvez de nouveau vous connecter.
