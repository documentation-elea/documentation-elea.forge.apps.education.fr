# Dictation H5P (Dictée autonome)

À partir d'un simple fichier son, vous pourrez avec ce contenu interactif (H5P) **Dictée autonome** créer des dictées autonomes : l'élève écoute et saisit au clavier ce qu'il entend ; puis son texte est alors automatiquement corrigé.

Ce contenu interactif est à employer préférablement avec des textes courts. Il favorise l'apprentissage des règles de grammaire et de l'orthographe en Lettres et Langues Étrangères. Mais il pourra être aussi mis à profit dans toute autre discipline dès qu'intervient un apprentissage d'un vocabulaire spécifique à connaître, ou, par exemple, de propriétés et de définitions à réciter.

Nous commencerons par créer un contenu interactif de type (en anglais) "**Dictation**".

![Icône de l'activité Dictation Dictée autonome](../img/imagesdictationdictee/dictationdicteeicone.png#printscreen#centrer)

## Ajouter le contenu interactif

1. Activez le mode édition dans votre parcours en cliquant sur ce bouton en haut à droite.

2. Cliquez dans la section souhaitée de votre parcours sur " **Ajouter une activité et ressource** " pour y créer une activité du type "**Contenu Interactif**".

![Bouton H5P](../img/creercontenusinteractifspartiecommune/H5Pn.png#printscreen#centrer)

Ou créez l'activité à partir de la banque de contenus.

![Bouton H5P](../img/creercontenusinteractifspartiecommune/H5Pb.png#printscreen#centrer)

3. Dans le formulaire qui s’affiche, après avoir saisi éventuellement une rapide description de l'activité qui va être créée, repérez le type de contenu interactif souhaité dans la section "**Éditeur**" et le menu "**H5P hub - Sélectionnez le type d'activité**" ; puis cliquez sur le bandeau correspondant pour créer la ressource voulue.

Si vous souhaitez davantage d'informations sur la procédure de création d'un contenu interactif, un tutoriel détaillé est disponible ici : [Tutoriel Créer un Contenu Interactif](https://dne-elearning.gitlab.io/moodle-elea/documentation/docs/Professeurs/Integrer%20des%20contenus%20interactifs/Creer_Contenus_Interactifs/).

## Créer la première phrase de la dictée autonome

![Interface de l'activité Dictation Dictée autonome](../img/imagesdictationdictee/dictationdictee01interface.png#printscreen#centrer)

1. Donnez un titre à l'activité. C'est sous ce titre que cette activité s'affichera dans le parcours Éléa.

2. Rédigez ici la consigne pour les élèves. 

**Exemple** : "Écoutez attentivement et écrivez ce que vous entendez."

3. ![Icone lecture normale](../img/imagesdictationdictee/dictationdicteeiconelecturenormale.png#printscreen)**Lecture normale** : Cliquez sur le signe **+** pour explorer vos fichiers et y sélectionner le fichier audio à déposer. Ce fichier contient donc une lecture à voix haute du texte dicté que l'élève aura à rédiger. 
Ce fichier son devra avoir été préalablement enregistré par l'enseignant de préférence au format mp3. 
On pourra par exemple utiliser à cette fin le logiciel [Audacity](https://audacity.fr/) pour une prise de son simplifiée et de qualité (voire même si nécessaire pour procéder à un montage rapide de ce fichier son). 
Alternativement vous pouvez aussi entrer l'adresse (url) pointant directement vers un fichier son qui est déjà hébergé en ligne ; par exemple en relevant l'adresse (url) du fichier son d'une dictée choisie sur le site [Dictée TV Monde](https://dictee.tv5monde.com/) adresse telle que celle-ci :
[https://vod.tv5monde.com/dictee/novembre2019/fantastique-A1-integrale.mp3](https://vod.tv5monde.com/dictee/novembre2019/fantastique-A1-integrale.mp3)
(adresse terminée par **.mp3**).
Cliquez ensuite sur "**Insérer**".
5. ![Icone lecture lente](../img/imagesdictationdictee/dictationdicteeiconelecturelente.png#printscreen)**Lecture lente** (optionnelle) : Il est possible en cliquant ici sur le signe **+** de prévoir un second fichier son qui contiendra lui aussi une lecture à voix haute du même texte que le fichier son précédent, mais où cette fois l'enseignant adoptera un débit délibérément ralenti, et ce afin de faciliter la saisie par l'élève du texte dicté.
Là encore, vous pouvez alternativement indiquer une adresse (url) pointant directement vers un tel fichier : dans l'exemple précédent des dictées proposées sur le site [Dictée TV Monde](https://dictee.tv5monde.com/) on sélectionnera par exemple l'adresse d'un fichier de lecture lente telle que celle-ci :
[https://vod.tv5monde.com/dictee/novembre2019/fantastique-A1-sequence01.mp3](https://vod.tv5monde.com/dictee/novembre2019/fantastique-A1-sequence01.mp3)
(adresse terminée par **.mp3**).
Cliquez ensuite sur "**Insérer**".
7. **Texte** : Dans ce champ de texte entrez le texte dicté tel qu'il est attendu que l'élève le saisisse. C'est ce texte de référence renseigné par l'enseignant qui permettra l'auto-correction de la dictée par comparaison avec le texte qui sera saisi par l'élève.

La dictée est prête : vous pouvez cliquer en bas de page sur "**ENREGISTRER ET AFFICHER**" pour la tester.

![Bouton enregistrer et afficher](../img/imagesdictationdictee/dictationdictee02boutonenregistreretafficher.png#printscreen#centrer)

## Créer les phrases suivantes
![Interface de l'activité Dictation Dictée autonome](../img/imagesdictationdictee/dictationdictee03boutonajouterphrase.png#printscreen#centrer)

Comme indiqué précédemment il est préférable d'employer des textes courts dans une telle activité de **Dictée autonome**.

Pour un texte plus long, on choisira donc généralement de segmenter la tâche, en offrant à l'élève une dictée phrase par phrase. Il sera nécessaire de prévoir autant de fichiers sons indépendants qu'il y aura de phrases dictées ainsi séparément.

1. Cliquez sur "**+ AJOUTER SENTENCE**" autant de fois que nécessaire pour ajouter autant de phrases que souhaité à la dictée. Pour chaque phrase, on recommencera les opérations précédentes (Ajout du fichier son de dictée et saisie du texte de correction ; ajout optionnel d'un fichier en lecture lente).
2. Pour naviguer entre les différentes phrases de la dictée, cliquez sur le titre de la phrase à modifier dans la barre grisée à gauche. Vous pouvez aussi modifier l'ordre des phrases de la dictée en cliquant sur les flèches ▲ et ▼ pour monter ou descendre une phrase par rapport aux autres. Cliquez sur la croix **x** pour supprimer définitivement une phrase de la dictée.

## Ajouter un média (optionnel)
Il est possible d'afficher en entête de l'activité une image ou une vidéo pour illustrer la **Dictée autonome**.

1. Cliquez sur la bandeau grisé "**Fichier média**".

![Bandeau Ficher Média](../img/imagesdictationdictee/dictationdictee04fichiermedia.png#printscreen#centrer)

2. Dans le menu déroulant "**Type de Média**" sélectionner "**Image**" ou "**Vidéo**" (ou sélectionnez le tiret ▬ pour supprimer éventuellement un média que vous auriez précédemment ajouté).

### Pour une image 

![Menu d'ajout d'un fichier image](../img/imagesdictationdictee/dictationdictee05fichiermediaimage.png#printscreen#centrer)

1. Cliquez sur le bouton "**+ Ajouter**". Puis parcourez les dossiers de votre ordinateur pour y sélectionner le fichier image souhaité. Un menu **Éditer l'image** apparaît : il permet de rogner l'image ou de la pivoter quart de tour par quart de tour.

![Bouton éditer l'image](../img/imagesdictationdictee/dictationdictee06boutonediterimage.png#printscreen#centrer)


![Menu d'édition d'image](../img/imagesdictationdictee/dictationdictee07editionimage.png#printscreen#centrer)

Enregistrez enfin les modifications appliquées à l'image.

![Bouton d'enregistrement d'une image](../img/imagesdictationdictee/dictationdictee08enregistrementimage.png#printscreen#centrer)


2. Rédigez un **Texte alternatif**  : ce texte se substituera à l'image si celle-ci rencontre un quelconque problème d'affichage quand l'activité est consultée.

3. (optionnel) **Texte de survol** : rédigez ici un texte qui s'affichera sous forme de bulle volante quand le pointeur de la souris survolera l'image.

4. **Désactiver l'agrandissement de l'image** : si cette option est cochée l'image sera affichée avec ses dimensions d'origine. En revanche en décochant cette option, une icône **+** et **-** dans le coin supérieur droit de l'image permettra à l'élève d'en faire varier les dimensions (zoom et dézoom).

![Exemple de zoom](../img/imagesdictationdictee/dictationdictee09zoomimage.png#printscreen#centrer)



### Pour une vidéo
![Menu d'ajout d'un fichier vidéo](../img/imagesdictationdictee/dictationdictee10fichiermediavideo.png#printscreen#centrer)

1. Indiquez un titre pour la vidéo qui précèdera la **Dictée autonome**.
2. Cliquez sur le signe **+** pour explorer vos fichiers et y sélectionner le fichier vidéo à déposer. Alternativement, vous pouvez aussi entrer directement l'adresse (url) d'une vidéo hébergée en ligne. Cliquez ensuite sur "**Insérer**".
3. Ce menu permet selon les options cochées :
      - d'insérer une image qui se substituera à la vidéo hors lecture.
      - de définir si le lecteur vidéo occupera la plus grande largeur possible dans la page où il s'affichera, ou s'il s'adaptera aux dimensions de la vidéo diffusée.
      -  de choisir si les contrôles de lecture, pause et arrêt apparaîtront ou non.
4. La lecture peut démarrer automatiquement au chargement du lecteur et/ou boucler une fois arrivée en fin de vidéo selon les options cochées dans ce menu.
5. Vous pouvez enfin déposer ici un fichier de sous-titres pour améliorer l'accessibilité de votre contenu vidéo. Ce fichier de sous-titres doit être au format WebVTT (pour en apprendre davantage sur ce format de fichier [cliquez ici](https://fr.wikipedia.org/wiki/WebVTT).

## Rédiger des rétroactions
![Menus des options](../img/imagesdictationdictee/dictationdictee11rétroactions.png#printscreen#centrer)

En cliquant sur "**Feedback général**" il est possible de rédiger des rétroactions qui s'afficheront à la fin de l'activité en fonction du score de l'élève. Par défaut une seule rétroaction est disponible qui s'affichera pour tous les scores pouvant être obtenus de 0% (aucune bonne réponse) à 100% (réussite complète). 

Cliquez sur "**Ajouter Intervalle**" autant de fois que souhaité pour indiquer de nouvelles valeurs de score qui viendront subdiviser cet intervalle initial couvrant de 0% à 100%. Pour chacune des nouvelles valeurs saisies entre 0 et 100, il sera alors possible de rédiger une rétroaction adaptée. Selon que le score obtenu sera en-deçà ou au-delà de cette valeur donnée (en %), c'est l'une ou autre de ces rétroactions qui s'affichera pour commenter le travail de l'élève au terme de l'activité.

## Comportement de l'activité et options des textes et boutons
![Interface des comportements](../img/imagesdictationdictee/dictationdictee12comportements.png#printscreen#centrer)

1. Fixez ici le nombre **maximal** d'écoutes possibles du fichier son en lecture **normale** dont l'élève pourra bénéficier pour réaliser cette dictée.
2. Fixez ici le nombre **maximal** d'écoutes possibles du fichier son en lecture **lente** dont l'élève pourra bénéficier pour réaliser cette dictée.
3. Si cette option est cochée la ponctuation ne sera pas prise en compte pour la correction.
4. Une marge de tolérance (exprimée en pourcentage) peut être ici fixée pour pallier les fautes de frappe.
5.  Si cette option est cochée, un bouton **Recommencer**" sera affiché pour l'élève après correction d'une première tentative.
6. Si cette option est cochée, un bouton "**Voir la solution**" sera affiché pour l'élève.

Enfin le menu "**Options et Textes**" permet par exemple de modifier les libellés des boutons de l’activité, ou encore les mentions affichées en cas de bonne ou mauvaise réponse, ainsi que les textes qui seront lus par un assistant de synthèse vocale pour une meilleure accessibilité de l'activité. Les mentions @score ou @total sont des mentions génériques auxquelles se substitueront respectivement le score effectivement obtenu par l'élève ou son nombre total d'erreurs pour une tentative donnée.

(Crédits images : pixabay.com)
