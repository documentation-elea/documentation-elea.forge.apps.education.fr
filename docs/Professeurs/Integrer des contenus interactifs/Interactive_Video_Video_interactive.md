# Interactive Video H5P (Vidéo interactive)

<iframe title="e-éducation &amp; Éléa - Intégrer une image interactive H5P" width="560" height="315" src="https://tube-numerique-educatif.apps.education.fr/videos/embed/bf12c589-3f96-46c9-aa76-b446270ca508" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

## Compléments d'informations à la vidéo :

## Ajouter le contenu interactif

1. Activez le mode édition dans votre parcours en cliquant sur ce bouton en haut à droite.

2. Cliquez dans la section souhaitée de votre parcours sur " **Ajouter une activité et ressource** " pour y créer une activité du type "**Contenu Interactif**".

![Bouton H5P](../img/creercontenusinteractifspartiecommune/H5Pn.png#printscreen#centrer)

Ou créez l'activité à partir de la banque de contenus.

![Bouton H5P](../img/creercontenusinteractifspartiecommune/H5Pb.png#printscreen#centrer)

3. Dans le formulaire qui s’affiche, après avoir saisi éventuellement une rapide description de l'activité qui va être créée, repérez le type de contenu interactif souhaité dans la section "**Éditeur**" et le menu "**H5P hub - Sélectionnez le type d'activité**" ; puis cliquez sur le bandeau correspondant pour créer la ressource voulue.

Si vous souhaitez davantage d'informations sur la procédure de création d'un contenu interactif, un tutoriel détaillé est disponible ici : [Tutoriel Créer un Contenu Interactif](https://dne-elearning.gitlab.io/moodle-elea/documentation/docs/Professeurs/Integrer%20des%20contenus%20interactifs/Creer_Contenus_Interactifs/).


## Intégrer une vidéo de Portail Tubes d'Apps-éducation

Connectez-vous au [portail Tubes](https://tubes.apps.education.fr/#) et sélectionnez la vidéo que vous voulez rendre interactive.

Sous la vidéo, cliquez sur les "**...**", puis cliquez sur **Télécharger**.

![Bouton H5P](../img/interactive_video/Capture1.png#printscreen#centrer)

 Choisissez la qualité vidéo qui vous convient (720p est un bon compromis), puis copiez le lien de la vidéo en cliquant sur le bouton **COPY**.

![Bouton H5P](../img/interactive_video/Capture2.png#printscreen#centrer)

 Le lien sera ensuite à coller dans le champ d'import de la vidéo (étape 1) dans votre activité H5P.

![Bouton H5P](../img/interactive_video/Capture3.png#printscreen#centrer)

## Intégrer une vidéo de PodEduc d'Apps-éducation

Connectez-vous au [portail PodEduc](https://podeduc.apps.education.fr/) et sélectionnez dans votre espace une vidéo que vous avez déposée.

Sous votre vidéo, vous trouvez un bouton de partage de la vidéo :

![Bouton H5P](../img/interactive_video/Capture4.png#printscreen#centrer)

En cliquant sur ce lien, une page apparaît dans laquelle vous allez pouvoir récupérer un lien permettant d'intégrer votre vidéo dans une activité H5P. Copiez le lien correspondant à la définition qui vous convient.

![Bouton H5P](../img/interactive_video/Capture5.png#printscreen#centrer)

 Le lien sera ensuite à coller dans le champ d'import de la vidéo (étape 1) dans votre activité H5P.

![Bouton H5P](../img/interactive_video/Capture3.png#printscreen#centrer)

