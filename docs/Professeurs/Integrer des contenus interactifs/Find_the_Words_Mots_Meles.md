# Find the Words H5P (Mots Mêlés)

Ce contenu interactif (H5P) permet de créer une grille de jeu du type "Mots Mêlés" : il s'agira pour l'élève de repérer puis d'entourer à la souris tous les mots qui ont été dissimulés dans une grille de lettres apparemment désordonnées qui s'affiche à l'écran. 

![Une grille de "Mots Mêlés"](../img/findthewordsmotsmeles/findthewordsmotsmeles01grillevierge.png#printscreen#centrer)

Ces mots auront été préalablement choisis par l'enseignant et se retrouveront écrits aussi bien à l'envers qu'à l'endroit, horizontalement, verticalement ou même en diagonale. 

À chaque nouvelle tentative les mots seront cachés différemment.

![Cette grille de "Mots Mêlés" en cours de résolution](../img/findthewordsmotsmeles/findthewordsmotsmeles02grilleenjeu.png#printscreen#centrer)

Nous commencerons par ajouter un contenu interactif du type (en anglais) "**Find the Words**".

![Icône de l'activité Find the Words Mots mêlés](../img/findthewordsmotsmeles/findthewordsmotsmelesicone.png#printscreen#centrer)

## Ajouter le contenu interactif

1. Activez le mode édition dans votre parcours en cliquant sur ce bouton en haut à droite.

2. Cliquez dans la section souhaitée de votre parcours sur " **Ajouter une activité et ressource** " pour y créer une activité du type "**Contenu Interactif**".

![Bouton H5P](../img/creercontenusinteractifspartiecommune/H5Pn.png#printscreen#centrer)

Ou créez l'activité à partir de la banque de contenus.

![Bouton H5P](../img/creercontenusinteractifspartiecommune/H5Pb.png#printscreen#centrer)

3. Dans le formulaire qui s’affiche, après avoir saisi éventuellement une rapide description de l'activité qui va être créée, repérez le type de contenu interactif souhaité dans la section "**Éditeur**" et le menu "**H5P hub - Sélectionnez le type d'activité**" ; puis cliquez sur le bandeau correspondant pour créer la ressource voulue.

Si vous souhaitez davantage d'informations sur la procédure de création d'un contenu interactif, un tutoriel détaillé est disponible ici : [Tutoriel Créer un Contenu Interactif](https://dne-elearning.gitlab.io/moodle-elea/documentation/docs/Professeurs/Integrer%20des%20contenus%20interactifs/Creer_Contenus_Interactifs/).

## Créer la grille de "Mots Mêlés"

![Présentation de l'Interface de l'activité "Mots Mêlés"](../img/findthewordsmotsmeles/findthewordsmotsmeles03interfacemotsmeles.png#printscreen#centrer)

1. Donnez un titre à la grille de jeu. C'est sous ce titre que cette activité s'affichera dans le parcours Éléa.
2. Rédigez la consigne pour les élèves.
3. Dressez la liste des mots qui seront dissimulés dans la grille. Les mots de cette liste doivent être séparés entre eux par une **virgule**. Ne laissez aucun espace vide dans cette liste. Vous ne pouvez pas utiliser de caractères spéciaux (type: $, &,#...), ni de chiffres.

Votre grille de jeu est terminée: vous pouvez cliquer en bas de page sur "**Enregistrer et afficher**" pour la tester.

![Bouton Enregistrer et Afficher](../img/findthewordsmotsmeles/findthewordsmotsmeles06enregistreretafficher.png#printscreen#centrer)



## Ajuster le niveau de difficulté de l'activité (optionnel).

1. Cliquez sur "**abcdefghijklmnopqrstuvwxyz**".

   ![Menu détaillé des options d'une activité Find the Words](../img/findthewordsmotsmeles/findthewordsmotsmeles04interfacedesoptions.png#printscreen#centrer)

2. Cliquez sur "**Orientations**" : cochez ou décochez ici les directions qui pourront être utilisées pour dissimuler les mots dans la grille de jeu. La difficulté de l'activité peut donc être ici modulée car selon les directions sélectionnées les mots s'avèreront plus ou moins faciles à détecter dans la grille finale.

3. Vous pouvez ici saisir la liste des lettres qui viendront, au moment de la création de la grille de jeu,  combler les espaces laissés vacants entre les mots cachés (par défaut l'alphabet complet est disponible). Le choix de ces lettres est donc aussi une manière de faire varier la difficulté de l'activité, selon que ces lettres "bouche-trous" sont nombreuses ou non (et donc qu'elles se répètent plus ou moins dans la grille), et selon qu'elles reprennent ou non des lettres des mots à découvrir.

4. Si cette option est cochée, les mots seront disposés dans la grille afin de privilégier un maximum de recoupement entre eux (lettres communes à plusieurs mots qui peuvent donc être partagées entre             ces mots entrecroisés). Il en résultera une grille de jeu plus compacte. Si l'option n'est pas cochée, la grille s'en trouvera élargie avec des mots cachés davantage espacés entre eux.

5. Si cette option est cochée, la liste des mots à découvrir sera visible pour l'élève à droite de la grille de jeu. Si l'option n'est pas cochée cette liste ne sera pas affichée (d'où il résultera une activité plus difficile).

6. Selon que cette option est cochée ou pas, le bouton "**Solutions**" sera ou non disponible pour que l'élève puisse faire s'afficher dans la grille tous les mots qui sont à y rechercher.

7. Selon que cette option est cochée ou pas, le bouton "**Réessayer**" sera ou non disponible pour que l'élève puisse lancer une nouvelle tentative. **Rappel** : à chaque nouvelle tentative une nouvelle grille de jeu est générée et les mots y sont donc cachés différemment à chaque fois.

![Menu Options et textes d'une activité Find the Words](../img/findthewordsmotsmeles/findthewordsmotsmeles05.png#printscreen#centrer)

Enfin, ce menu "**Options et Textes**" permet par exemple de modifier les libellés des boutons de l'activité,  ou encore la rédaction de la rétroaction de fin d'activité : "Vous avez marqué tant de points sur tant". 

Afin que cette rétroaction soit adaptée en fonction de chaque tentative de l'élève, une partie de ce texte est générique. Il sera réactualisé à chaque affichage. **Exemple** : à la mention **@score** viendra en fait se substituer, au moment de l'affichage, le nombre qui correspondra au score effectivement obtenu par l'élève lors d'une tentative donnée.
