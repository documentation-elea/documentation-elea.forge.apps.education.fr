# Image Pairing H5P (Appariement d'images)

Ce contenu interactif (H5P) propose une activité d'appariement, les deux éléments de chaque paire à retrouver étant chacun une image. Les deux séries d'images se présenteront en deux colonnes : l'élève devra cliquer-déplacer les images de la colonne de gauche sur l'image correspondante dans la colonne de droite.

![Exemple d'appariemment d'images](../img/imagespairingappariementimages/imagespairingappariementimages00exemple.png#printscreen#centrer)

L'activité pourra aussi permettre d'associer par exemple des illustrations (schémas, photographies, tableaux, etc.) et leurs légendes (c'est-à-dire des textes mais enregistrés ici sous forme d'images).

Nous commencerons par créer un contenu interactif de type (en anglais) "**Image Pairing**".

![Icône de l'activité Image Pairing Appariement d'Images](../img/imagespairingappariementimages/imagespairingappariementimagesicone.png#printscreen#centrer)

## Ajouter le contenu interactif

1. Activez le mode édition dans votre parcours en cliquant sur ce bouton en haut à droite.

2. Cliquez dans la section souhaitée de votre parcours sur " **Ajouter une activité et ressource** " pour y créer une activité du type "**Contenu Interactif**".

![Bouton H5P](../img/creercontenusinteractifspartiecommune/H5Pn.png#printscreen#centrer)

Ou créez l'activité à partir de la banque de contenus.

![Bouton H5P](../img/creercontenusinteractifspartiecommune/H5Pb.png#printscreen#centrer)

3. Dans le formulaire qui s’affiche, après avoir saisi éventuellement une rapide description de l'activité qui va être créée, repérez le type de contenu interactif souhaité dans la section "**Éditeur**" et le menu "**H5P hub - Sélectionnez le type d'activité**" ; puis cliquez sur le bandeau correspondant pour créer la ressource voulue.

Si vous souhaitez davantage d'informations sur la procédure de création d'un contenu interactif, un tutoriel détaillé est disponible ici : [Tutoriel Créer un Contenu Interactif](https://dne-elearning.gitlab.io/moodle-elea/documentation/docs/Professeurs/Integrer%20des%20contenus%20interactifs/Creer_Contenus_Interactifs/).


## Créer la première paire d'images

![Interface de l'activité Flashcards Cartes flash](../img/imagespairingappariementimages/imagespairingappariementimages02interface.png#printscreen#centrer)

1. Donnez un titre à l'activité. C'est sous ce titre que cette activité s'affichera dans le parcours Éléa.

2. Rédigez ici la consigne de l'activité. 

**Exemple** : "Déplacez chaque animal sur ses empreintes."

3. **Image** : Cliquez sur le bouton "**+ Ajouter**". Puis parcourez les dossiers de votre ordinateur pour y sélectionner le fichier souhaité d'une première image.
**Remarque** : pour offrir le meilleur affichage possible à vos élèves prévoir des images de format **carré** (idéalement redimensionnées au format 105 pixels par 105 pixels).
5. Rédigez ici un **Texte alternatif**  : ce texte se substituera à l'image si celle-ci rencontre un quelconque problème d'affichage quand l'activité est consultée.
    ![Interface de création de la première image d'une paire](../img/imagespairingappariementimages/imagespairingappariementimages03interface.png#printscreen#centrer)
    5.et 6. **Image Correspondante** : recommencez les deux dernières étapes pour l'image qui devra être associée à la précédente.

![Interface de création de la deuxième image d'une paire](../img/imagespairingappariementimages/imagespairingappariementimages04interface.png#printscreen#centrer)

Pour supprimer une image (en vue de lui en substituer une autre par exemple), cliquez sur la croix **x** en haut à droite de celle-ci.

![Icône de suppression d'image](../img/imagespairingappariementimages/imagespairingappariementimages05suppresseonimage.png#printscreen#centrer)

Votre première paire d'images à apparier est prête : vous pouvez cliquer en bas de page sur "**ENREGISTRER ET AFFICHER**" pour la tester.

![Bouton enregistrer et afficher](../img/imagespairingappariementimages/imagespairingappariementimages06boutonenregistreretafficher.png#printscreen#centrer)

## Créer et organiser les paires d'images suivantes

![Interface d'ajout et de navigation des paires d'images](../img/imagespairingappariementimages/imagespairingappariementimages07ajoutetnavigationentrelespaires.png#printscreen#centrer)

1. Cliquez sur "**+ AJOUTER CARTE**" autant de fois que nécessaire pour ajouter à la série autant de paires d'images supplémentaires que souhaité. Pour chaque nouvelle paire on recommencera les quatre opérations précédentes (ajout des deux images à associer entre elles et rédaction du texte alternatif de substitution pour chacune).
2. Pour naviguer entre les différentes paires d'images, cliquez sur le titre de la paire à modifier dans la barre grisée à gauche. Vous pouvez aussi modifier  dans l'activité l'ordre des paires entre elles, en cliquant sur les flèches ▲ et ▼ pour monter ou descendre une paire d'images par rapport aux autres. Cliquez sur la croix **x** pour supprimer définitivement une paire d'images.

## Éditer les images (optionnel)

Sous chaque image ajoutée un menu **Éditer l'image** apparaît : il permet de rogner l'image ou de la pivoter quart de tour par quart de tour.

![Bouton éditer l'image](../img/imagespairingappariementimages/imagespairingappariementimages08boutonediterimage.png#printscreen#centrer)


![Menu d'édition d'image](../img/imagespairingappariementimages/imagespairingappariementimages09menueditionimages.png#printscreen#centrer)

Enregistrez enfin les modifications appliquées à l'image.

![Bouton d'enregistrement d'une image](../img/imagespairingappariementimages/imagespairingappariementimages10enregistrerimage.png#printscreen#centrer)

**Remarque** : un bouton copyright est aussi associé à chaque image de l'album pour en renseigner les crédits et droits d'usage. Pour un rappel des principales licences en vigueur [cliquez ici](http://creativecommons.fr/licences/).

![Le bouton pour renseigner les crédits et droits d'usage des images qui apparaissent dans l'album.](../img/imagespairingappariementimages/imagespairingappariementimages11boutoncopyright.png#printscreen#centrer)

## Bouton recommencer et options

![Bouton recommencer](../img/imagespairingappariementimages/imagespairingappariementimages12comportementsetoptions.png#printscreen#centrer)

1. En cochant cette option, l'élève disposera d'un bouton "**Recommencer**" pour relancer une nouvelle tentative au terme de l'activité.

2. Enfin le menu "**Options et Textes**" permet  de modifier les libellés des textes et boutons de l'activité. Les mentions @score ou @total sont des mentions génériques auxquelles se substitueront respectivement, le score effectivement obtenu par l'élève pour une tentative donnée, et le nombre de points qui pouvaient être obtenus au total dans l'activité créée.
