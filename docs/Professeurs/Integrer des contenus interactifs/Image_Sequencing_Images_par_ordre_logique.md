# Image Sequencing H5P (img par ordre logique)

Avec ce contenu interactif (H5P) l'élève se voit proposer une série d'images sur un même thème : il s'agira alors par glisser-déposer à la souris d'ordonner ces images entre elles selon l'ordre qui est spécifié dans la consigne.

Ce contenu interactif est donc adapté à l'étude de toute notion qui inclut une dimension chronologique : par exemple remettre en ordre des illustrations en fonction d'un récit déjà connu de l'élève en Lettres ou en Histoire ; ordonner les étapes à respecter d'un protocole d'expérience en Sciences ; ou bien encore classer des œuvres pour une période donnée en Histoire de l'Art. 

![Exemple d'une activité de séquence d'images : cycle de vie d'une plante](../img/imagesequencingsequenceimages/imagesequencingsequenceimages01exempleactivité.png#printscreen#centrer)

Nous commencerons par créer un contenu interactif de type (en anglais) "**Image Sequencing**".

![Icone de l'activité image sequencing Images par ordre logique](../img/imagesequencingsequenceimages/imagesequencingsequenceimagesicone.png#printscreen#centrer)

## Ajouter le contenu interactif

1. Activez le mode édition dans votre parcours en cliquant sur ce bouton en haut à droite.

2. Cliquez dans la section souhaitée de votre parcours sur " **Ajouter une activité et ressource** " pour y créer une activité du type "**Contenu Interactif**".

![Bouton H5P](../img/creercontenusinteractifspartiecommune/H5Pn.png#printscreen#centrer)

Ou créez l'activité à partir de la banque de contenus.

![Bouton H5P](../img/creercontenusinteractifspartiecommune/H5Pb.png#printscreen#centrer)

3. Dans le formulaire qui s’affiche, après avoir saisi éventuellement une rapide description de l'activité qui va être créée, repérez le type de contenu interactif souhaité dans la section "**Éditeur**" et le menu "**H5P hub - Sélectionnez le type d'activité**" ; puis cliquez sur le bandeau correspondant pour créer la ressource voulue.

Si vous souhaitez davantage d'informations sur la procédure de création d'un contenu interactif, un tutoriel détaillé est disponible ici : [Tutoriel Créer un Contenu Interactif](https://dne-elearning.gitlab.io/moodle-elea/documentation/docs/Professeurs/Integrer%20des%20contenus%20interactifs/Creer_Contenus_Interactifs/).

## Créer la consigne

![Interface d'édition de l'activité Image Sequencing Images par ordre logique](../img/imagesequencingsequenceimages/imagesequencingsequenceimages02interface.png#printscreen#centrer)

1. Donnez un titre à l'activité. C'est sous ce titre que cette activité s'affichera dans le parcours Éléa.

2. Rédigez la consigne qui spécifie l'ordre attendu pour le classement des images proposées.

**Exemple** : Cliquez-déplacez ces images pour les ordonner entre elles : vous devez recréer le cycle de vie de cette plante, de la germination à la pollinisation.

3. Rédigez à nouveau cette consigne : ce second libellé ne sera pas affiché, mais c'est lui qui sera lu par les outils d'accessibilité améliorée par synthèse vocale.

**Exemple** : Ordonnez ces images correctement. Utilisez les flèches du clavier pour naviguer parmi les images, utilisez la barre espace pour sélectionner ou désélectionner une image puis déplacez-la avec les flèches du clavier.

## Créer la première image de la séquence

![Interface d'édition d'une image dans l'activité Image Sequencing Images par ordre logique](../img/imagesequencingsequenceimages/imagesequencingsequenceimages03interfaceimage.png#printscreen#centrer)

1. Cliquez sur "**+ Ajouter**". Puis parcourez les dossiers de votre ordinateur pour y sélectionner le fichier image souhaité. 

**Remarque** : en pratique il est conseillé d'avoir redimensionné auparavant toutes les images devant être incluses dans la séquence, et ce afin d'obtenir un format d'affichage uniforme (idéalement un format quasi-carré de 165 pixels de large sur 160 pixels de haut est à privilégier).

2. Rédigez un **Texte alternatif**  : ce texte descriptif se substituera à l'image si celle-ci rencontre un quelconque problème d'affichage quand l'activité est consultée.

3. (optionnel) **Fichier Son** : cliquez sur le **+** puis parcourez les dossiers de votre ordinateur pour y sélectionner un fichier son qui sera associé à l'image en question. Ce fichier son peut, par exemple, permettre à l'élève d'écouter la prononciation d'un terme représenté dans l'image ; ou encore, il peut reprendre la description de celle-ci afin d'améliorer l'accessibilité de l'activité aux personnes déficientes visuelles.

![Icone effacer image](../img/imagesequencingsequenceimages/imagesequencingsequenceimages13iconeeffacerimage.png#printscreen#centrer)

Une image peut être supprimée (afin par exemple de lui en substituer une autre), en cliquant sur l'icone croix **x** en haut à droite.

## Compléter la séquence avec les images suivantes

![Barre de titres des images de la séquence](../img/imagesequencingsequenceimages/imagesequencingsequenceimages04interfaceimage.png#printscreen#centrer)

1.Pour ajouter chaque nouvelle image et en modifier les paramétrages, cliquez sur son titre dans la barre grisée à gauche. Recommencez pour chaque nouvelle image ajoutée les étapes précédentes ("**Ajouter**", "**Texte Alternatif**" et éventuellement ajout d'un fichier son). 

2.Par défaut la séquence prévoit d'inclure trois images : cliquez sur "**AJOUTER IMAGE**" pour ajouter une à une à la séquence autant de nouvelles images que souhaité. 

**Attention** : l'ordre, **de haut en bas**, dans lequel sont ajoutées les images (qui se retrouvent automatiquement numérotées 1, 2, 3 etc.) définit l'**ordre attendu comme étant la bonne réponse** à l'activité de classement de cette activité d'**Images par ordre logique**.

Autrement dit, au lancement de l'activité, cet ordre sera aléatoirement modifié pour l'affichage initial des images, et ce sera donc à l'élève de reconstituer cet ordre voulu.

Votre activité **Images par ordre logique** est prête : vous pouvez cliquer en bas de page sur "**Enregistrer et afficher**" pour la tester.

![Bouton Enregistrer et afficher](../img/imagesequencingsequenceimages/imagesequencingsequenceimages05boutonenregistreretafficher.png#printscreen#centrer)

## Modifier la séquence d'images

![Icones de déplacement et suppression des images de la séquence](../img/imagesequencingsequenceimages/imagesequencingsequenceimages06deplacementsuppressionimages.png#printscreen#centrer)

Les images de la séquence peuvent être déplacées entre elles : montées dans la liste en cliquant à gauche sur **▲**, ou descendues en cliquant sur **▼**. 

En conséquence ceci modifiera automatiquement l'ordre attendu comme étant la bonne réponse à l'activité.

Cliquer sur la croix **x** permet de supprimer une image donnée.

## Modifier les images de l'album

![Bouton éditer image](../img/imagesequencingsequenceimages/imagesequencingsequenceimages07boutonediterimage.png#printscreen#centrer)

Un menu "**Éditer l'image**" apparaît sous chaque image ajoutée : il permet de rogner cette image ou encore de la pivoter quart de tour par quart de tour.

![Menu d'édition des images](../img/imagesequencingsequenceimages/imagesequencingsequenceimages08menueditionimages.png#printscreen#centrer)

Enregistrez enfin les modifications appliquées à l'image.

![Le bouton pour enregistrer les modifications de l'image](../img/imagesequencingsequenceimages/imagesequencingsequenceimages09enregistrerimage.png#printscreen#centrer)

**Remarque** : un bouton copyright est aussi associé à chaque image de l'album pour en renseigner les crédits et droits d'usage. Pour un rappel des principales licences en vigueur [cliquez ici](http://creativecommons.fr/licences/).

![Bouton de copyright](../img/imagesequencingsequenceimages/imagesequencingsequenceimages10boutoncopyright.png#printscreen#centrer)

## Sélectionner les boutons et les options

![Interface des boutons et options](../img/imagesequencingsequenceimages/imagesequencingsequenceimages11boutonsetoptions.png#printscreen#centrer)

Cliquez sur la flèche ► pour dérouler le menu de gestion d'affichage des trois boutons disponibles pour l'activité : cochez la case correspondant aux boutons que vous souhaitez voir affichés pour l'élève.

1. Le bouton "**Solution**" : l'élève peut voir la solution de l'activité, c'est-à-dire les images correctement réordonnées entre elles. Cette option est à privilégier dans une optique d'auto-évaluation formative.

2. Le bouton "**Recommencer**" : une fois l'activité terminée l'élève peut lancer une nouvelle tentative.

3. Un bouton "**Reprendre**" sera proposé à l'élève à chaque fois qu'il lance une vérification de son travail : des coches rouges et vertes lui indiquent si les images sont convenablement placées ou non dans la séquence attendue.

![Exemple de correction intermédiaire](../img/imagesequencingsequenceimages/imagesequencingsequenceimages12boutonrecommencer.png#printscreen#centrer)

(Le menu déroulant "**Options et Textes**" permet enfin de modifier les libellés qui seront lus par les outils d'accessibilité améliorée par synthèse vocale).

(crédits images : vecteurs "Cycle de vie d'une plante" par brgfx pour freepik.com)
