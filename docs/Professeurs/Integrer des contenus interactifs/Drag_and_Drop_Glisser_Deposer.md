# Drag and drop H5P (Glisser-Déposer)

<iframe title="e-éducation &amp; Éléa - Réaliser une activité glisser - déposer avec H5P" width="560" height="315" src="https://tube-numerique-educatif.apps.education.fr/videos/embed/42ccfc30-dc81-4575-a655-922a4249136e" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></iframe>

Dans ce contenu interactif l'élève disposera d'éléments qui peuvent être des images ou bien des textes ; éléments qu'il s'agira de glisser-déposer à la souris sur les endroits corrects prédéfinis au-dessus d'une image d'arrière-plan.

![Exemple d'activité de Drag and drop Glisser-Déposer](../img/imagesdraganddropglisserdeposer/draganddropglisserdeposer01exemple.png#printscreen#centrer)

Ici, par exemple, l'élève doit légender correctement le schéma en déplaçant les noms des huit planètes du système solaire. Ces étiquettes nominatives (de type "textes") peuvent être déposées sur des "zones de dépôt" qui ont été définies préalablement par l'enseignant (lequel contrôle ainsi par avance les endroits de l'image susceptibles ou non de recevoir un ou plusieurs des éléments à déplacer).

![Exemple d'activité de Drag and drop Glisser-Déposer](../img/imagesdraganddropglisserdeposer/draganddropglisserdeposer02exemple.png#printscreen#centrer)

Nous commencerons par créer un contenu interactif de type (en anglais) "**Drag and Drop**".

![Icône de l'activité Drag and drop Glisser-Déposer](../img/imagesdraganddropglisserdeposer/draganddropglisserdeposericone.png#printscreen#centrer)

## Ajouter le contenu interactif

1. Activez le mode édition dans votre parcours en cliquant sur ce bouton en haut à droite.

2. Cliquez dans la section souhaitée de votre parcours sur " **Ajouter une activité et ressource** " pour y créer une activité du type "**Contenu Interactif**".

![Bouton H5P](../img/creercontenusinteractifspartiecommune/H5Pn.png#printscreen#centrer)

Ou créez l'activité à partir de la banque de contenus.

![Bouton H5P](../img/creercontenusinteractifspartiecommune/H5Pb.png#printscreen#centrer)

3. Dans le formulaire qui s’affiche, après avoir saisi éventuellement une rapide description de l'activité qui va être créée, repérez le type de contenu interactif souhaité dans la section "**Éditeur**" et le menu "**H5P hub - Sélectionnez le type d'activité**" ; puis cliquez sur le bandeau correspondant pour créer la ressource voulue.

Si vous souhaitez davantage d'informations sur la procédure de création d'un contenu interactif, un tutoriel détaillé est disponible ici : [Tutoriel Créer un Contenu Interactif](https://dne-elearning.gitlab.io/moodle-elea/documentation/docs/Professeurs/Integrer%20des%20contenus%20interactifs/Creer_Contenus_Interactifs/).

## Créer l'arrière-plan de l'activité

![Interface de l'activité Drag and drop Glisser-Déposer](../img/imagesdraganddropglisserdeposer/draganddropglisserdeposer05interface.png#printscreen#centrer)

1. Donnez un titre à l'activité. C'est sous ce titre que cette activité s'affichera dans le parcours Éléa.

La création d'une activité "**Glisser-Déposer**" se déroule en deux temps :

- On téléversera d'abord l'image d'arrière-plan qui servira de support visuel à l'activité (onglet 2).

- Puis on créera les éléments à déplacer ainsi que les "zones de dépôt" destinées à recevoir ces éléments mobiles (éléments qui peuvent être de type textes ou bien images) (onglet 4).

  

2. **Image d'arrière-plan** : cet onglet permet de déposer l'image d'arrière-plan de l'activité. 

3. Cliquez sur le bouton "**+ Ajouter**". Puis parcourez les dossiers de votre ordinateur pour y sélectionner le fichier image souhaité. C'est cette image qui structurera l'activité en fournissant en arrière-plan à l'élève des indications qui le guideront pour faire ses choix de dépôts des éléments à déplacer au-dessus de cette image de fond.

(pour des paramétrages plus avancés de cette image d'arrière-plan voir infra : "**Comment bien choisir son image d'arrière-plan**" et "**Options pour les images**")

4. Cliquez enfin sur une des mentions "**Éléments de l'activité**" pour passer à l'onglet suivant et poursuivre la création de l'activité. 

## Créer les éléments de l'activité

Les éléments de l'activité sont :

- d'une part les éléments à déplacer (de type textes ou images).
- d'autre part les "zones de dépôt" où devront être correctement placés les éléments précédents.

**Recommandation importante** : Pour une élaboration facilitée de l'activité **Glisser-Déposer** il est préférable de respecter l'ordre de création suivant :

1. créer et nommer toutes les "zones de dépôt" nécessaires au-dessus de l'image d'arrière-plan.

2. puis créer et nommer tous les éléments à déplacer vers ces "zones de dépôt".

3. enfin revenir paramétrer lesdites "zones de dépôt" afin d'indiquer pour chacune d'elles quel(s) élement(s) y est ou y sont attendu(s).

C'est cette mise en relation **zone de dépôt\<-\>élément(s) correct(s)** qui permettra la correction automatique de l'activité : or cette mise en relation se paramètre grâce aux **noms** attribués aux "zones de dépôt" et aux éléments mobiles. Suivre l'ordre de création recommandé ici permettra donc de dénommer tous les éléments utiles **avant** de créer les correspondances **zone de dépôt\<-\>élément(s) correct(s)** voulues.

## Créer des éléments "zones de dépôt"

Dans l'onglet "**Éléments de l'activité**" cliquez sur le bouton de création d'une nouvelle zone de dépôt.

![Bouton créer une zone de dépôt](../img/imagesdraganddropglisserdeposer/draganddropglisserdeposer06iconezonededepot.png#printscreen#centrer)

![Interface de création d'une zone de dépôt](../img/imagesdraganddropglisserdeposer/draganddropglisserdeposer07interfacezonededepot.png#printscreen#centrer)

1. **Étiquette** : Nommez la zone de dépôt qui va être créée. Pour rappel ce nom sera employé pour définir plus tard les relations entre cette "zone de dépôt" et le ou les élément(s) correct(s) qui devront y être déposés ; aussi il est conseillé de choisir un nom significatif pour chaque "zone de dépôt" créée, et ce afin par la suite de facilement les identifier et distinguer les unes des autres.

2. En cochant cette option le nom choisi pour la "zone de dépôt" sera dans l'activité rendu visible au-dessus de celle-ci. 

   **Remarque** : En pratique, l'enseignant(e) peut souhaiter temporairement afficher les noms de toutes ces "zones de dépôt" : ceci permettra de les distinguer clairement pour organiser leur positionnement final au-dessus de l'image d'arrière-plan ; avant, ensuite, de supprimer cet affichage des noms dans l'activité finale destinée à l'élève.

3. On peut aussi choisir de rendre pour l'élève plus ou moins apparente la "zone de dépôt" dans l'activité (de l'opacité totale : 100 ; à la transparence complète : 0). L'enseignant(e) peut en effet souhaiter dans certaines activités que la "zone de dépôt" soit partiellement transparente pour que l'élève puisse encore voir au travers d'elle l'image d'arrière-plan en transparence.

4. En cliquant sur "**Aides et commentaires**" l'enseignant(e) peut renseigner trois champs (optionnels) :
   - rédiger un indice que l'élève pourra afficher en cliquant sur une icone![Icône indice](../img/imagesdraganddropglisserdeposer/draganddropglisserdeposer08indice.png#printscreen#centrer).
   - rédiger une rétroaction qui s'affichera si un élément est **correctement** déposé sur cette "zone de dépôt".
   - rédiger une rétroaction qui s'affichera si un élément est **incorrectement** déposé sur cette "zone de dépôt".

5. Par défaut une "zone de dépôt" peut être destinataire de plusieurs éléments corrects. En cochant cette option on indique donc qu'au contraire un élément et un seul est à déposer sur cette  zone.

6. Cocher cette option (dans le cas où plusieurs éléments peuvent être déposés sur une seule et même "zone de dépôt") permettra  au fur et à mesure de l'activité d'ajuster automatiquement entre eux leurs positions dans la zone en question, et ce afin de conserver la clarté de lecture de l'ensemble en évitant tout chevauchement intempestif entre les éléments qui doivent être déposés en un même endroit.

Cliquez enfin sur **OK** pour créer cette "zone de dépôt".

Créez autant de "zones de dépôt" que nécessaire en cliquant pour chacune sur ce bouton.

![Bouton créer une zone de dépôt](../img/imagesdraganddropglisserdeposer/draganddropglisserdeposer06iconezonededepot.png#printscreen#centrer)

## Créer des éléments textes à déplacer

Dans l'onglet "**Éléments de l'activité**" cliquez sur le bouton de création d'un nouvel élément texte.

![Icone de création d'un élément texte](../img/imagesdraganddropglisserdeposer/draganddropglisserdeposer09iconeelementtexte.png#printscreen#centrer)

![Interface de création d'un élément texte](../img/imagesdraganddropglisserdeposer/draganddropglisserdeposer10interfaceelementtexte.png#printscreen#centrer)

1.**Texte** : Rédigez ici le texte de l'élément, texte que l'élève lira pour décider où le glisser et le déposer. Pour rappel ce libellé sera employé  plus tard pour indiquer les "zones de dépôt" où cet élément pourra être correctement déposé. 

2."**Sélectionnez les zones de dépôt**" : apparait ici la liste nominative de toutes les "zones de dépôt" précédemment créées. Cochez celles qui devront être susceptibles d'accueillir cet élément texte (il est possible de cocher en entête "**Sélectionner tout**" pour indiquer que cet élément texte pourra être déposé sur toutes les "zones de dépôt" présentes dans l'activité).

**Attention : les choix cochés ici ne déterminent pas la correction de l'activité.** Ces choix décrivent seulement le comportement de l'activité : si l'élément est déplacé sur une "zone de dépôt" **qui n'est pas cochée ici**, il en sera automatiquement **repoussé** et retournera vers sa position de départ. Si la "zone de dépôt" est en revanche ici cochée, l'élément y restera en place (mais encore une fois cela ne présume en rien que ce dépôt est correct : la correction sera définie ultérieurement dans les paramétrages des "zones de dépôt" - voir infra "**Mise en relation des "zones de dépôt" et des éléments mobiles**").

3. On peut aussi choisir de rendre pour l'élève plus ou moins apparent cet élément texte dans l'activité (de l'opacité totale : 100 ; à la transparence complète : 0). L'enseignant(e) peut en effet souhaiter dans certaines activités que l'élément texte soit partiellement transparent pour qu'à travers lui l'élève puisse encore voir l'image d'arrière-plan afin de mieux apprécier où glisser-déposer exactement ce texte mobile.

4. Si par exemple plusieurs élément de l'image d'arrière-plan doivent revoir une seule et même légende commune, on ne créera qu'une seule fois celle-ci : mais cet élément texte, en cochant cette case, sera rendu disponible en autant d'exemplaires que voulu par l'élève.

Cliquez enfin sur **OK** pour créer cet élément texte.

Créez autant d'éléments textes que nécessaire en cliquant pour chacun sur ce bouton.

![Icone de création d'un élément texte](../img/imagesdraganddropglisserdeposer/draganddropglisserdeposer09iconeelementtexte.png#printscreen#centrer)

## Créer des éléments images à déplacer

Dans l'onglet "**Éléments de l'activité**" cliquez sur le bouton de création d'un nouvel élément texte.

![Icone de création d'un élément image](../img/imagesdraganddropglisserdeposer/draganddropglisserdeposer11iconeelementimage.png#printscreen#centrer)

![Interface de création d'un élément image](../img/imagesdraganddropglisserdeposer/draganddropglisserdeposer12interfacecreationelementimage.png#printscreen#centrer)

1. Cliquez sur "**+ Ajouter**". Puis parcourez les dossiers de votre ordinateur pour y sélectionner le fichier image souhaité. 

**Remarque** : en pratique il est conseillé d'avoir préalablement redimensionné toutes les images devant être incluses dans l'activité ; il restera toutefois possible d'éditer plus tard ces images dans l'activité (voir infra "**Positionner et dimensionner les éléments de l'activité"** et **"Options pour les images**").

2. Rédigez un **Texte alternatif**  : ce texte se substituera à l'image si celle-ci rencontre un quelconque problème d'affichage quand l'activité est consultée.

3. (optionnel) **Texte de survol** : rédigez ici un texte qui s'affichera sous forme de bulle volante quand le pointeur de la souris survolera cette image.

4. "**Sélectionnez les zones de dépôt**" : apparait ici la liste nominative de toutes les "zones de dépôt" précédemment créées. Cochez celles qui devront être susceptibles d'accueillir cet élément image (il est possible de cocher en entête "**Sélectionner tout**" pour indiquer que cet élément image pourra être déposé sur toutes les "zones de dépôt" présentes dans l'activité).

**Attention : les choix cochés ici ne déterminent pas la correction de l'activité.** Ces choix décrivent seulement le comportement de l'activité : si l'élément est déplacé sur une "zone de dépôt" **qui n'est pas cochée ici**, il en sera automatiquement **repoussé** et retournera vers sa position de départ. Si la "zone de dépôt" est en revanche ici cochée, l'élément y restera en place (mais encore une fois cela ne présume en rien que ce dépôt est correct : la correction sera définie ultérieurement dans les paramétrages des "zones de dépôt" - voir infra "**Mise en relation des "zones de dépôt" et des éléments mobiles**").

3. On peut aussi choisir de rendre pour l'élève plus ou moins apparent cet élément image dans l'activité (de l'opacité totale : 100 ; à la transparence complète : 0). L'enseignant(e) peut en effet souhaiter dans certaines activités que l'élément image soit partiellement transparent pour qu'à travers lui l'élève puisse encore voir l'image d'arrière-plan afin de mieux apprécier où glisser-déposer exactement cette image mobile.

4. Si un élément image doit être employé à plusieurs reprises pour compléter l'activité, on n'en créera qu'un seul : mais cet élément image, en cochant cette case, sera rendu disponible en autant d'exemplaires que voulu par l'élève.

Cliquez enfin sur **OK** pour créer cet élément image.

Créez autant d'éléments images que nécessaire en cliquant pour chacun sur ce bouton.

![Icone de création d'un élément image](../img/imagesdraganddropglisserdeposer/draganddropglisserdeposer11iconeelementimage.png#printscreen#centrer)

## Positionner et dimensionner les éléments de l'activité

Les éléments "zone de dépôt", textes et images mobiles peuvent être déplacés au-dessus de l'image d'arrière-plan par cliquer-glisser, et peuvent être redimensionnés en tirant sur les "poignées" carrées blanches du pourtour de l'élément sélectionné.

![Poignées de dimensionnement](../img/imagesdraganddropglisserdeposer/draganddropglisserdeposer16poigneesdimensionnement.png#printscreen#centrer)

**Remarque**: en maintenant la **touche majuscule** du clavier enfoncée et en jouant sur la  poignée inférieure droite de l'élément choisi, celui-ci sera redimensionné de manière proportionnelle en longueur et largeur à ses dimensions d'origine. Il est aussi possible d'utiliser les quatre flèches du clavier pour déplacer précisément l'élément sélectionné.

Il s'agit d'une part de définir la position de chaque "zone de dépôt" en cohérence visuellement avec l'image d'arrière-plan choisie ; et de fixer d'autre part la positon **initiale** de chaque élément texte et image mobile.

Pour un positionnement ou un redimensionnement plus précis on utilisera les outils du menu flottant de l'élément sélectionné.

![Menu flottant de position et dimensions](../img/imagesdraganddropglisserdeposer/draganddropglisserdeposer17menuflottantpositiondimensions.png#printscreen#centrer)

## Paramétrer la correction de l'activité : mise en relation des "zones de dépôt" et des éléments mobiles

Pour créer la correction de l'activité il va s'agir de sélectionner une à une chacun des "zones de dépôt" puis d'en modifier les paramètres en cliquant sur l'icone stylo.

![Icone d'édition d'une "zone de dépôt"](../img/imagesdraganddropglisserdeposer/draganddropglisserdeposer18iconeedition.png#printscreen#centrer)

Dans l'interface de la "zone de dépôt" apparaît désormais le liste nominative de tous les éléments mobiles (textes et images) pour lesquels on avait précédemment indiqué qu'ils pouvaient être bien déposés sur cette « zone de dépôt » (voir supra "**Créer des éléments textes à déplacer**" et "**Créer des éléments images à déplacer**").

![Interface de correction](../img/imagesdraganddropglisserdeposer/draganddropglisserdeposer19correction.png#printscreen#centrer)

Si les éléments listés ici peuvent être donc tous déposés a priori sur la "zone de dépôt" sélectionnée (sans en être "repoussés"), en revanche **seuls ceux qui seront effectivement cochés dans cette liste seront alors considérés comme des réponses correctes pour ce dépôt**.

L'activité **Glisser-déposer** est prête : vous pouvez cliquer en bas de page sur "**ENREGISTRER ET AFFICHER**" pour la tester.

![Bouton enregistrer et afficher](../img/imagesdraganddropglisserdeposer/draganddropglisserdeposer20boutonenregistreretafficher.png#printscreen#centrer)

## Comment bien choisir son image d'arrière-plan

Au départ de l'activité les éléments à glisser-déposer s'afficheront d'emblée au-dessus de l'image d'arrière-plan (à leur positon initiale qui diffère des endroits -"zone de dépôt"- où il s'agira finalement pour l'élève de les glisser-déposer). 

Aussi il faudra toujours bien penser à prévoir dans cette image d'arrière-plan **un espace "neutre"** ; espace destiné donc à y "stocker" initialement les éléments qui seront déplacés ensuite sur le reste de l'image. 

Dans cet exemple une "marge" a ainsi été ménagée sous le titre "Système Solaire" car c'est là qu'au démarrage de l'activité sont répertoriés les noms des huit planètes servant à légender ensuite le reste du schéma.

![Exemple d'image d'arrière-plan](../img/imagesdraganddropglisserdeposer/draganddropglisserdeposer21exempleimagearriereplan.png#printscreen#centrer)

De la même manière il s'agira de prévoir les espaces dans cette image au-dessus desquels viendront se positionner ensuite les "zones de dépôt".

![Réglage de la taille de l'image d'arrière-plan](../img/imagesdraganddropglisserdeposer/draganddropglisserdeposer23tailleimagearriereplan.png#printscreen#centrer)

**Remarque** : **Taille de la zone d'activité** : il s'agit en fait ici moins de définir la taille en pixels à proprement parler de l'activité que d'en indiquer les proportions (c'est-à-dire le ratio d'affichage de l'image de fond ). En effet **l'activité occupera par défaut la plus grande largeur de la page** où elle s'affichera. Dans cet exemple  l'image d'arrière-plan choisie est de format carré : aussi que l'on saisisse ici 500x500 ou bien 10x10 l'affichage résultant sera exactement le même, à savoir un format carré. Afin donc d'éviter toute déformation de l'image d'arrière-plan dans l'activité finale, il est recommandé de saisir ici simplement les dimensions exactes (ou des multiples et sous-multiples de celles-ci) de l'image elles-mêmes (note : les mentions portées ici peuvent affecter en revanche les dimensions de l'image pour l'enseignant(e) lors de la création de l'activité : choisir un multiple ou un sous-multiple des dimensions initiales de l'image peut donc permettre de zoomer ou de dézoomer dans l'image d'arrière-plan pour un meilleur confort de lecture pendant la création de l'activité).

Du fait que **l'activité occupera par défaut la plus grande largeur de la page** un **format oblong** pour l'image arrière-plan est à préférer avec une résolution convenable : l'activité sera ainsi réduite en hauteur et offrira un meilleur confort d'affichage et une ergonomie accrue à l'élève pour la manipulation des éléments mobiles.

**Remarque** : pour obtenir ce format oblong il pourra être astucieux comme ici de faire le choix de délibérément laisser des marges blanches de part et d'autre de la partie centrale de l'image de fond.

![Exemple marges pour l'image d'arrière-plan](../img/imagesdraganddropglisserdeposer/draganddropglisserdeposer22exemplemargesimagearriereplan.png#printscreen#centrer)

Avec ces réglages.

![Exemple de taille d'image d'arrière-plan avec marges](../img/imagesdraganddropglisserdeposer/draganddropglisserdeposer24tailleimagearriereplanavecmarges.png#printscreen#centrer)

## Options pour les images

Chaque image ajoutée à l'activité peut être supprimée (afin par exemple de lui en substituer une autre) avec l'icone croix **x** en haut à droite.

![Icône de suppression d'image](../img/imagesdraganddropglisserdeposer/draganddropglisserdeposer25suppressionimage.png#printscreen#centrer)

Sous chaque image ajoutée à l'activité un menu **Éditer l'image** apparaît : il permet de rogner l'image ou de la pivoter quart de tour par quart de tour.

![Bouton éditer l'image](../img/imagesdraganddropglisserdeposer/draganddropglisserdeposer26boutonediterimage.png#printscreen#centrer)


![Menu d'édition d'image](../img/imagesdraganddropglisserdeposer/draganddropglisserdeposer27menueditionimages.png#printscreen#centrer)

Enregistrez enfin les modifications appliquées à l'image :

![Bouton d'enregistrement d'une image](../img/imagesdraganddropglisserdeposer/draganddropglisserdeposer28enregistrerimage.png#printscreen#centrer)

**Remarque** : un bouton copyright est aussi associé à chaque image de l'activité pour en renseigner les crédits et droits d'usage. Pour un rappel des principales licences en vigueur [cliquez ici](http://creativecommons.fr/licences/).

![Le bouton pour renseigner les crédits et droits d'usage des images qui apparaissent dans l'album.](../img/imagesdraganddropglisserdeposer/draganddropglisserdeposer29boutoncopyright.png#printscreen#centrer)

## Rédiger des rétroactions

![Interface des rétroactions](../img/imagesdraganddropglisserdeposer/draganddropglisserdeposer30rétroactions.png#printscreen#centrer)

En cliquant sur "**Feedback général**" il est possible de rédiger des rétroactions qui s'afficheront à la fin de l'activité en fonction du score de l'élève. Par défaut une seule rétroaction est disponible qui s'affichera pour tous les scores pouvant être obtenus de 0% (aucune bonne réponse) à 100% (réussite complète). 

Cliquez sur "**Ajouter Intervalle**" autant de fois que souhaité pour indiquer de nouvelles valeurs de score qui viendront subdiviser cet intervalle initial couvrant de 0% à 100%. Pour chacune des nouvelles valeurs saisies entre 0 et 100, il sera alors possible de rédiger une rétroaction adaptée. Selon que le score obtenu sera en-deçà ou au-delà de cette valeur donnée (en %), c'est l'une ou autre de ces rétroactions qui s'affichera pour commenter le travail de l'élève au terme de l'activité.

## Options de l'activité Glisser-Déposer

![Interface des options de l'activité Drag and drop Glisser-déposer](../img/imagesdraganddropglisserdeposer/draganddropglisserdeposer31options.png#printscreen#centrer)

1. Cocher cette case pour permettre à l'élève de relancer une nouvelle tentative au terme de l'activité en cas d'erreur(s).
2. En cochant cette case le bouton "**Vérifier**" est actif dès que l'élève a effectué un premier glisser-déposer.
3. En cochant cette option la totalité de l'activité rapportera un point (sinon chaque dépôt correct est comptabilisé séparément)
4. Cette option entraîne attribution d'un point négatif pour chaque dépôt erroné.
5. Une icone d'information apparaîtra au-dessus du score si cette option est cochée : elle donne accès à une courte explication des points positifs et des pénalités appliquées au score de l'élève.
6. Il est possible de fixer globalement le degré d'opacité-100/transparence-0 de toutes les "zones de dépôt" de l'activité.
7. En choisissant ici l'option "**Lors du glisser-déposer**" toutes les "zones de dépôt" susceptibles d'accueillir un élément donné se signalent en surbrillance pendant son déplacement. Avec l'option "**Jamais**"" l'affichage des zones reste inchangé et sans surbrillance ; avec l'option "**Toujours**" la surbrillance des "zones de dépôt" est permanente. 
8. Lorsque plusieurs éléments peuvent être déposés sur une même zone de dépôt ce paramètre définit l'espacement entre eux pour leur alignement automatique.
9. En cochant cette option l'activité pourra être affichée en plein écran : cette option est particulièrement appréciable pour ce type d'activité **Glisser-déposer** car celle-ci exige précisément de l'élève de nombreuses manipulations à l'écran.
10. Si cette option est cochée en plus du score global en bas d'activité, le score propre à chaque "zone de dépôt" est détaillé au-dessus de celle-ci lors de la vérification.
11. On choisira ici si le titre donné à l'activité sera affiché ou non au-dessus de celle-ci.

(crédit image : freepik.com)
