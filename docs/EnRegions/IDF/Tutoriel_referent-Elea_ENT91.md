# Créer les comptes utilisateurs avec l'ENT 91

## Etape 1 - Exporter vos comptes utilisateurs depuis l'ENT

Connectez-vous à https://moncollege-ent.essonne.fr avec vos identifiants puis - dans la liste des applications - choisissez **Administration** (seuls les administrateurs voient cette icône).

![administration](ent91/capture1.png) puis ![administration](ent91/admin-icon.png)

La console d'administration apparaît à l'écran :

![exports](ent91/console.png)

Cliquez sur **Exporter des comptes** dans la section **Imports / Exports**.

Dans le nouvel écran qui s'affiche :

![export](ent91/export.png)

1. Rendez-vous dans l'onglet **Exporter des comptes** ;
2. Vérifiez que les valeurs reprennent bien celles de la capture ci-dessus ;
3. Cliquez sur **Exporter**.

Il ne vous reste plus qu’à enregistrer le fichier **« export.csv »** à l’endroit que vous souhaitez.

![docs_à_télécharger](ent91/capture4.png)

**Très important** : Le fichier doit impérativement se nommer  **« export.csv »** et vous devrez le détruire à la fin de la procédure ! 

Si le fichier se nomme autrement (par exemple **« export (1).csv »**), c'est que vous avez sans doute omis de supprimer le précédent export réalisé sur votre machine. Supprimez alors le plus vieux et renommez le plus récent en supprimant les parenthèses et le chiffre à l'intérieur.


## Etape 2 - Se connecter à la plateforme Elea de l'établissement

Un courriel automatique est adressé au chef d'établissement / IEN via l'adresse **UAI@ac-versailles.fr**.
Il contient le lien permettant **d'activer le compte local de gestion de l'établissement** sur la plateforme Éléa, puis d'y **importer les comptes utilisateurs.**

Entrez l'url correspondant à votre plateforme de bassin sur le modèle **BASSIN.elea.ac-versailles.fr**

Cliquez sur « **Choisissez votre compte** » puis sur « **Utiliser mon compte local** ».

![../accueil_elea](../accueil_elea/capture2_91.png)

Puis entrez **l'UAI (RNE) et le mot de passe.**

![../accueil_elea](../accueil_elea/capture3.png)

## Etape 3 - Importer vos comptes utilisateurs dans Elea

Sur la page de gestion de votre établissement, l'onglet "Accueil" vous donne l'historique de vos derniers imports.

![import](../import/import_accueil.png#printscreen#centrer)

Cliquez sur l'onglet **« Importer des utilisateurs »**.

Puis cliquez sur **« Sélectionnez un fichier sur votre ordinateur »** ou **glissez/déposez** celui-ci.

![import](../import/import_etape1.png#printscreen#centrer)

Patientez pendant la création des cohortes et des utilisateurs. La procédure est automatique et ne demande aucune action de votre part.

![création cohortes](../import/import_etape2.png#printscreen#centrer)

![import utilisateurs](../import/import_etape3.png#printscreen#centrer)

Attendez l’affichage du message **« importation terminée ».**

![importation_terminée](../import/import_etape4.png#printscreen#centrer)
