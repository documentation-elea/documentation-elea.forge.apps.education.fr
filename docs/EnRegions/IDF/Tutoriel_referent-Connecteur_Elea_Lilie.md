import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

## Créer un connecteur sur MonLycée.net
:::tip Bon à savoir
Le délai entre la création du connecteur et son bon fonctionnement peut varier de quelques minutes à plusieurs heures. Un peu de patience avant de tester.
:::


### Accéder à la console

Connectez-vous à l'ENT avec un compte administrateur. Si vous l'êtes bien, vous trouverez l'icône __Administration__ dans vos applications pour lancer la console :

![admin-icon](./entidf/admin-icon.png#printscreen#centrer)

Une fois dans celle-ci, cliquez sur le lien « Gérer les connecteurs » de la rubrique « Services » :

![Services Administration](./ent_edifice/admin_services.png#printscreen#centrer)

*Autre méthode, cliquez sur le lien suivant : https://ent.iledefrance.fr/appregistry/admin-console !*

### Créer un connecteur

Cliquez sur le bouton « Créer un connecteur »

![Créer un connecteur](./ent_edifice/creer_connecteur.png#printscreen#centrer)

### Paramétrer votre connecteur

#### 1. Téléversez un fichier pour l'icône :


![Téléverser l'icône](./ent_edifice/upload_icone.png#printscreen#centrer)

![Icône ÉLÉA](./ent_edifice/logo_elea.png#printscreen#centrer) 

(ou renseignez l'url : `https://communs.elea.apps.education.fr/theme/elea/pix/logo_elea.png`)

#### 2. Utilisez le tableau ci-dessous pour compléter les autres paramètres :


<Tabs queryString="action">
<TabItem value="Pour l'académie de Versailles" label="Pour l'académie de Versailles">

   ![Paramètres](./ent_edifice/parametres_lien.png#printscreen#centrer)

   | Paramètre       | Valeur                                                                   |
   |-----------------|--------------------------------------------------------------------------|
   | Identifiant     | éléa suivi de votre UAI/RNE (avec accents, minuscules obligatoires et sans espace)               |
   | Nom d'affichage | ÉLÉA                                                                     |
   | URL             | `https://[bassin].elea.ac-versailles.fr/login/index.php?multicas=entidf` |
   | Cible           | Nouvelle page                                                            |

:::danger Attention
 L'URL doit être adaptée à votre situation en vous aidant du tableau ci-dessous.
   :::

Exemple d'URL valide : https://**pontoise**.elea.ac-versailles.fr/login/index.php?multicas=entidf

**Adresse url bassin Académie de Versailles**

| Nom          | Adresse                                                                       |
|--------------|-------------------------------------------------------------------------------|
|Antony        | https://antony.elea.ac-versailles.fr/login/index.php?multicas=entidf          |
|Argenteuil    | https://argenteuil.elea.ac-versailles.fr/login/index.php?multicas=entidf      |
|Boulogne      | https://boulogne.elea.ac-versailles.fr/login/index.php?multicas=entidf        |
|Cergy         | https://cergy.elea.ac-versailles.fr/login/index.php?multicas=entidf           |
|Enghien       | https://enghien.elea.ac-versailles.fr/login/index.php?multicas=entidf         |
|Etampes       | https://etampes.elea.ac-versailles.fr/login/index.php?multicas=entidf         |
|Évry          | https://evry.elea.ac-versailles.fr/login/index.php?multicas=entidf            |
|Gennevilliers | https://gennevilliers.elea.ac-versailles.fr/login/index.php?multicas=entidf   |
|Gonesse       | https://gonesse.elea.ac-versailles.fr/login/index.php?multicas=entidf         |
|Mantes        | https://mantes.elea.ac-versailles.fr/login/index.php?multicas=entidf          |
|Massy         | https://massy.elea.ac-versailles.fr/login/index.php?multicas=entidf           |
|Montgeron     | https://montgeron.elea.ac-versailles.fr/login/index.php?multicas=entidf       |
|Mureaux       | https://mureaux.elea.ac-versailles.fr/login/index.php?multicas=entidf         |
|Nanterre      | https://nanterre.elea.ac-versailles.fr/login/index.php?multicas=entidf        |
|Neuilly       | https://neuilly.elea.ac-versailles.fr/login/index.php?multicas=entidf         |
|Poissy        | https://poissy.elea.ac-versailles.fr/login/index.php?multicas=entidf          |
|Pontoise      | https://pontoise.elea.ac-versailles.fr/login/index.php?multicas=entidf        |
|Rambouillet   | https://rambouillet.elea.ac-versailles.fr/login/index.php?multicas=entidf     |
|Sarcelles     | https://sarcelles.elea.ac-versailles.fr/login/index.php?multicas=entidf       |
|Savigny       | https://savigny.elea.ac-versailles.fr/login/index.php?multicas=entidf         |
|Saint-Germain | https://sgl.elea.ac-versailles.fr/login/index.php?multicas=entidf             |
|Saint-Quentin | https://sqy.elea.ac-versailles.fr/login/index.php?multicas=entidf             |
|Vanves        | https://vanves.elea.ac-versailles.fr/login/index.php?multicas=entidf          |
|Versailles    | https://versailles.elea.ac-versailles.fr/login/index.php?multicas=entidf      |


</TabItem>
<TabItem value="Pour les académies de Créteil et Paris" label="Pour les académies de Créteil et Paris">

    ![Paramètres](./ent_edifice/parametres_lien.png)

   | Paramètre       | Valeur                                                                                   |
   |-----------------|------------------------------------------------------------------------------------------|
   | Identifiant     | éléa suivi de votre UAI/RNE (sans espace, sans majuscule)                                |
   | Nom d'affichage | ÉLÉA                                                                                     |
   | URL             | `https://[identifiant_plateforme].elea.apps.education.fr/login/index.php?multicas=entidf`|
   | Cible           | Nouvelle page                                                                            |

   **ATTENTION** : L'URL doit être adaptée à votre situation en vous aidant du tableau ci-dessous.
   
Exemple d'URL valide : https://**idf-77-1**.elea.apps.education.fr/login/index.php?multicas=entidf

**Adresse url bassin Académie de Créteil**

| Nom                 | Adresse                                                                  |
| ------------------- | ------------------------------------------------------------------------ |
| 77 Nord-Ouest       | https://idf-77-1.elea.apps.education.fr/login/index.php?multicas=entidf  |
| 77 Centre           | https://idf-77-2.elea.apps.education.fr/login/index.php?multicas=entidf  |
| 77 Sud              | https://idf-77-3.elea.apps.education.fr/login/index.php?multicas=entidf  |
| 77 Sud-Ouest        | https://idf-77-4.elea.apps.education.fr/login/index.php?multicas=entidf  |
| 93 Nord-Ouest       | https://idf-93-1.elea.apps.education.fr/login/index.php?multicas=entidf  |
| 93 Nord-Est         | https://idf-93-2.elea.apps.education.fr/login/index.php?multicas=entidf  |
| 93 Sud-Ouest        | https://idf-93-3.elea.apps.education.fr/login/index.php?multicas=entidf  |
| 93 Sud-Est          | https://idf-93-4.elea.apps.education.fr/login/index.php?multicas=entidf  |
| 94 Est              | https://idf-94-1.elea.apps.education.fr/login/index.php?multicas=entidf  |
| 94 Centre           | https://idf-94-2.elea.apps.education.fr/login/index.php?multicas=entidf  |
| 94 Ouest            | https://idf-94-3.elea.apps.education.fr/login/index.php?multicas=entidf  |

**Adresse url bassin Académie de Paris**

| Nom                 | Adresse                                                                    |
| ------------------- | -------------------------------------------------------------------------- |
| Paris Nord          | https://idf-75-nord.elea.apps.education.fr/login/index.php?multicas=entidf |
| Paris Est           | https://idf-75-est.elea.apps.education.fr/login/index.php?multicas=entidf  |
| Paris Sud           | https://idf-75-sud.elea.apps.education.fr/login/index.php?multicas=entidf  |
| Paris Ouest         | https://idf-75-ouest.elea.apps.education.fr/login/index.php?multicas=entidf|


</TabItem>
</Tabs> 

#### 3. Configurez le champ spécifique CAS

Cliquez sur le menu « Champs spécifiques CAS »

![Champs spécifiques CAS](./ent_edifice/champs_cas.png#printscreen#centrer)

Cochez la case « Activer le champ spécifique CAS »
Sélectionnez « Défaut » dans le menu déroulant « Type »

#### 4. Validez la création

N'oubliez pas de cliquer sur le bouton « Créer »

![Créer](./ent_edifice/bouton_creer.png#printscreen#centrer)

#### 5. Attribuer les droits

Pour terminer, attribuez les droits d'accès à ce connecteur via l'onglet "attribution" :

![Attribution des droits](./ent_edifice/attribution.png#printscreen#centrer)

Vous devriez donner l'accès aux profils suivants :

- Tous les enseignants
- Tous les élèves
- Tous les personnels

:::info
*Les parents n'ont pas accès à Éléa puisqu'ils n'ont pas vocation à créer des parcours et qu'il n'est pas attendu qu'ils réalisent les exercices.*
:::
