# Créer un connecteur Éléa sur Oze

Le connecteur Éléa est mis en place directement par l'éditeur Itop (image ci-dessous). S'il n'apparaît pas, veuillez contacter directement l'éditeur.
L'icône Éléa apparaît sur le menu et est accessible par l'ensemble des utilisateurs de l'ENT à l'exception des parents qui n'ont pas accès à la plateforme.

![Connecteur](./ent_oze/tuile_elea.png)

En cas de déconnexion en cours d'utilisation de la plateforme Éléa, il sera nécessaire de vous reconnecter en passant par l'ENT Oze. 
Il subsiste, en effet, pour le moment une erreur de contexte utilisateur. Un correctif est en attente auprès de l'éditeur.

![erreur contexte utilisateur](./ent_oze/erreur_contexte_utilisateur.png)

