## Étape 1 - Exporter vos comptes utilisateurs depuis l'ENT

**Connectez-vous à votre ENT** avec un compte administrateur puis cliquez sur le menu bleu en haut à droite.

![Mes Oz'apps](ent_oze/menu.png)

Dans **« Mes Oz'Apps favorites »**, cliquez sur **« Annuaire »**.

![Mes Oz'apps](ent_oze/OzApps.png)

Cliquez sur **« AFFICHER TOUS LES UTILISATEURS »**.

![Profil](ent_oze/Annuaire_profil.png)

Puis sur **« Exporter »** et enfin sélectionnez **« SACoche(CSV) »**.

![Exporter](ent_oze/Export_SACoche.png)

Enregistrez le fichier.

![Enregistrer](ent_oze/Enregistrer_sous.png)

**Remarque** : Pour des questions juridiques, tous les fichiers exportés doivent être **détruits** dès la fin de la procédure.  


## Étape 2 - Se connecter à la plateforme Éléa de l'établissement

Un courriel automatique est adressé au chef d'établissement / IEN via l'adresse **UAI@ac-versailles.fr**.
Il contient le lien permettant d'activer le compte local de gestion de l'établissement sur la plateforme Éléa, puis d'y importer les comptes utilisateurs.

Entrez l'url correspondant à votre plateforme de bassin sur le modèle **BASSIN.elea.ac-versailles.fr**

Cliquez sur « **Choisissez votre compte** » puis sur « **Utiliser mon compte local** ».

![accueil_elea](../accueil_elea/capture2_92.png)

Puis entrez **l'UAI (RNE) et le mot de passe.**

![accueil_elea](../accueil_elea/capture3.png)

## Étape 3 - Importer vos comptes utilisateurs dans Éléa

Sur la page de gestion de votre établissement, l'onglet "Accueil" vous donne l'historique de vos derniers imports.

![import](../import/import_accueil.png#printscreen#centrer)

Cliquez sur l'onglet **« Importer des utilisateurs »**.

Puis cliquez sur **« Sélectionnez un fichier sur votre ordinateur »** ou **glissez/déposez** celui-ci.

![import](../import/import_etape1.png#printscreen#centrer)

Patientez pendant la création des cohortes et des utilisateurs. La procédure est automatique et ne demande aucune action de votre part.

![création cohortes](../import/import_etape2.png#printscreen#centrer)

![import utilisateurs](../import/import_etape3.png#printscreen#centrer)

Attendez l’affichage du message **« importation terminée ».**

![importation_terminée](../import/import_etape4.png#printscreen#centrer)

