# Créer un connecteur Éléa sur l'ENT "Webcollege"

## Accéder à la liste des services

Connectez-vous à l'ENT avec les identifiants d'un compte ayant les pouvoirs d'administrateur pour votre établissement

Dans le menu de gauche, cliquez sur **ADMINISTRATION**.

![](../AuRA/img/capture2.png#printscreen#centrer)

Un menu secondaire apparaît et indique que vous vous trouvez dans les sous-sections **Services > Liste des services**. La liste des services existants s'affiche à droite de l'écran.

![](../AuRA/img/capture10.png#printscreen#centrer)

## Créer un nouveau connecteur

Cliquez sur **Nouveau service** en haut à droite.

![](../AuRA/img/capture11.png#printscreen#centrer)

Complétez les seuls champs **Type de SSO**, **URL** et **Intitulé** en vous aidant de la capture et du tableau suivants :

![](../AuRA/img/capture12.png#printscreen#centrer)

| Paramètre   | Valeur                                                       |
| ----------- | ------------------------------------------------------------ |
| Type de SSO | Pas de SSO ou SSO standard                                   |
| URL         | voir ci-dessous |
| Intitulé    | Éléa                                                         |

:::info **Remarques**
 L'URL ci-dessus est uniquement valable pour les établissements du bassin de Pontoise. Vous devez l'adapter à votre situation en vous aidant du tableau en annexe.
Vous pouvez modifier **Regroupement** pour que le connecteur apparaisse aux utilisateurs dans une autre catégorie. Pourquoi pas dans **Pédagogie** ou **Ressources numériques** ?
:::

## Partager votre connecteur

Cliquez sur l'onglet **Accès population**.

![](../AuRA/img/capture13.png#printscreen#centrer)

1. Sélectionnez le rôle **Accès au service** dans la première liste déroulante ;
2. Sélectionnez le profil **Elève** dans la seconde ;
3. Cliquez sur **Ajouter les accès**.
4. Recommencez les étapes 2 et 3 avec les profils **Enseignant**, **Non enseignant** et **Autre**.

![](../AuRA/img/capture14.png#printscreen#centrer)

> **Remarque** : les parents et/ou tuteurs n'accèdent pas à Éléa.

## Annexe - URL des bassins concernés

| Nom                                   | Adresse                                                      |
| -------------------------------- | ------------------------------------------------------------ |
| Nord-Ouest                       | https://idf-93-1.elea.apps.education.fr/login/index.php?authCAS=KOS93       |
| Nord-Est                         | https://idf-93-2.elea.apps.education.fr/login/index.php?authCAS=KOS93       |
| Sud-Ouest                        | https://idf-93-3.elea.apps.education.fr/login/index.php?authCAS=KOS93       |
| Sud-Est                          | https://idf-93-4.elea.apps.education.fr/login/index.php?authCAS=KOS93       |