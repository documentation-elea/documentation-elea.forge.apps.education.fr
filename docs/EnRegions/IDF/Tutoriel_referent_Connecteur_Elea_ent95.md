# Créer un connecteur Éléa sur moncollege.valdoise.fr

## Accéder à la liste des services

Connectez-vous à l'ENT avec les identifiants d'un compte ayant les pouvoirs d'administrateur pour votre établissement : https://cas.moncollege.valdoise.fr

Avant de continuer, vous aurez peut-être besoin de sélectionner votre établissement dans la liste déroulante qui apparaît en haut à droite de l'écran lorsque vous cliquez sur **Mes ENT**.

![](ent95/capture1.png#printscreen#centrer)

Dans le menu de gauche, cliquez sur **ADMINISTRATION**.

![](ent95/capture9.png#printscreen#centrer)

Un menu secondaire apparaît et indique que vous vous trouvez dans les sous-sections **Services > Liste des services**. La liste des services existants s'affiche à droite de l'écran.

![](ent95/capture10.png#printscreen#centrer)

## Créer un nouveau connecteur

Cliquez sur **Nouveau service** en haut à droite.

![](ent95/capture11.png#printscreen#centrer)

Complétez les seuls champs **Type de SSO**, **URL** et **Intitulé** en vous aidant de la capture et du tableau suivants :

![](ent95/capture12.png#printscreen#centrer)

| Paramètre   | Valeur                                                       |
| ----------- | ------------------------------------------------------------ |
| Type de SSO | Pas de SSO ou SSO standard                                   |
| URL         | https://pontoise.elea.ac-versailles.fr/login/index.php?authCAS=KOS95 |
| Intitulé    | Éléa                                                         |

> __Remarques__ :
>
> L'URL ci-dessus est uniquement valable pour les établissements du bassin de Pontoise. Vous devez l'adapter à votre situation en vous aidant du tableau en annexe.
>
> Vous pouvez modifier **Regroupement** pour que le connecteur apparaisse aux utilisateurs dans une autre catégorie. Pourquoi pas dans **Pédagogie** ou **Ressources numériques** ?

## Partager votre connecteur

Cliquez sur l'onglet **Accès population**.

![](ent95/capture13.png#printscreen#centrer)

1. Sélectionnez le rôle **Accès au service** dans la première liste déroulante ;
2. Sélectionnez le profil **Elève** dans la seconde ;
3. Cliquez sur **Ajouter les accès**.
4. Recommencez les étapes 2 et 3 avec les profils **Enseignant**, **Non enseignant** et **Autre**.

![](ent95/capture14.png#printscreen#centrer)

> **Remarque** : les parents et/ou tuteurs n'accèdent pas à Éléa.

## Annexe - URL des bassins concernés

| Nom           | Adresse                                                      |
| ------------- | ------------------------------------------------------------ |
| Argenteuil    | https://argenteuil.elea.ac-versailles.fr/login/index.php?authCAS=KOS95 |
| Cergy         | https://cergy.elea.ac-versailles.fr/login/index.php?authCAS=KOS95 |
| Enghien       | https://enghien.elea.ac-versailles.fr/login/index.php?authCAS=KOS95 |
| Gonesse       | https://gonesse.elea.ac-versailles.fr/login/index.php?authCAS=KOS95 |
| Pontoise      | https://pontoise.elea.ac-versailles.fr/login/index.php?authCAS=KOS95 |
| Sarcelles     | https://sarcelles.elea.ac-versailles.fr/login/index.php?authCAS=KOS95 |
