# Créer un connecteur Éléa sur e-Collège

## Accéder à la console

Connectez-vous à l'ENT avec un compte administrateur. Si vous l'êtes bien, vous trouverez l'icône __Administration__ dans vos applications pour lancer la console.

![admin-icon](ent78/bouton.png)

## Créer un connecteur

1. Cliquez sur __Gérer les connecteurs__.

  ![console](ent78/console.png)

2. Puis en haut à droite sur __Créer un connecteur__.

   ![connecteur](ent78/connecteur.png)

## Paramétrer votre connecteur

Utilisez le tableau et la capture d'écran ci-dessous pour compléter les champs.

| Paramètre       | Valeur                                                       |
| --------------- | ------------------------------------------------------------ |
| URL de l'icône  | https://idf-77-1.elea.apps.education.fr/theme/vital/pix/logo.png |
| Identifiant     | __elea__ suivi de votre __UAI/RNE__ (sans espace, sans majuscule) |
| Nom d'affichage | Éléa                                                         |
| URL             | https://idf-77-1.elea.apps.education.fr/login/index.php?authCAS=ENT77 |
| Cible           | Nouvelle page                                                |

__ATTENTION__ : Ces deux liens sont valables uniquement pour les établissements du bassin de Saint-Germain en Laye. Vous devez les adapter à votre situation en vous aidant du tableau en annexe !



![admin-cas-params](ent78/parametres.png)

En dessous, dans "Champs spécifiques CAS", activez le champs spécifique CAS, puis choisir comme type Eléa.

![admin-cas-params](ent78/parametres2.png)

Cliquez enfin sur le bouton **Créer**.

![Bouton Créer](ent91/boutoncreerconnecteur.png)

Remarques : 
- Il existe un temps de latence pendant lequel l'erreur INVALID_SERVICE peut apparaître.
- L'administrateur doit se déconnecter de l'ENT avant de tester le connecteur.
- Si vous apportez ultérieurement des modifications sur cette page, cliquez alors en haut à droite sur __Enregistrer__.

![save](ent78/save.png)

## Activer les droits

Vous devez maintenant décider qui pourra accéder à ce connecteur. Rendez-vous dans **Attribution**.

![droits](ent78/droits.png)

Vous devriez donner l'accès aux profils suivants :

* Tous les enseignants
* Tous les élèves
* Tous les personnels

*Les parents n'ont pas accès à Éléa puisqu'ils n'ont pas vocation à créer des parcours et qu'il n'est pas attendu qu'ils réalisent les exercices ;-)*

## Annexe - adresse URL de chaque bassin

| Nom          | Adresse                                                                 |
| ------------ | ----------------------------------------------------------------------- |
| Nord-Ouest   | https://idf-77-1.elea.apps.education.fr/login/index.php?authCAS=ENT77   |
| Centre       | https://idf-77-2.elea.apps.education.fr/login/index.php?authCAS=ENT77   |
| Sud          | https://idf-77-3.elea.apps.education.fr/login/index.php?authCAS=ENT77   |
| Sud-Ouest    | https://idf-77-4.elea.apps.education.fr/login/index.php?authCAS=ENT77   |