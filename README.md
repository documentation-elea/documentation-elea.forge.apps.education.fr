### Pour la procédure

lire [Readme du produit](README_DOCUSAURUS.md)

### Visualiser le rendu

[ÉléaDoc](https://dne-elearning.gitlab.io/moodle-elea/documentation)

### On en parle

[FORUM Docusaurus IAM](https://iam.unistra.fr/mod/forum/view.php?id=4493)